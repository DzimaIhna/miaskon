$(document).ready(function(){
    var productCount = $('.product-item').length;
    $('.nav-link').click( function(){
        var scroll_el = $(this).attr('href');
            if ($(scroll_el).length != 0) {
            $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 1000);
            }
            return false;
    });
    $('.slick').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 5000,
        fade: true,
        cssEase: 'linear'
    });
    $('.header-slick').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 2000,
		fade: true,
        cssEase: 'linear'
    });
    //$('header').css('height',window.innerHeight);
    for(var i = 0; i < productCount; i++){
        $($('.product-item')[i]).css('z-index', productCount - i - 1);
    }
    $('.form-control').attr('required', 'required');
});
-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 27 2018 г., 14:18
-- Версия сервера: 5.7.21
-- Версия PHP: 7.0.27-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `miaskonby`
--

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_active_users`
--

CREATE TABLE `aw36_active_users` (
  `sid` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `action` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `id` int(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data about last user action.';

--
-- Дамп данных таблицы `aw36_active_users`
--

INSERT INTO `aw36_active_users` (`sid`, `internalKey`, `username`, `lasthit`, `action`, `id`) VALUES
('ee4bb0129b496fdd1c15c64437f81a41', 2, 'des', 1524823038, '8', NULL),
('ac8dc9117bbfa832432939f24c2491a0', 2, 'des', 1524824714, '67', 3),
('cb8344ef6685920eabe3d40abb21c284', 1, 'admin', 1524821203, '8', NULL),
('3bf95339c502cda16e333bf09e204d07', 2, 'des', 1524822694, '8', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_active_user_locks`
--

CREATE TABLE `aw36_active_user_locks` (
  `id` int(10) NOT NULL,
  `sid` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `elementType` int(1) NOT NULL DEFAULT '0',
  `elementId` int(10) NOT NULL DEFAULT '0',
  `lasthit` int(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data about locked elements.';

--
-- Дамп данных таблицы `aw36_active_user_locks`
--

INSERT INTO `aw36_active_user_locks` (`id`, `sid`, `internalKey`, `elementType`, `elementId`, `lasthit`) VALUES
(14, 'ac8dc9117bbfa832432939f24c2491a0', 2, 7, 19, 1524824595);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_active_user_sessions`
--

CREATE TABLE `aw36_active_user_sessions` (
  `sid` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data about valid user sessions.';

--
-- Дамп данных таблицы `aw36_active_user_sessions`
--

INSERT INTO `aw36_active_user_sessions` (`sid`, `internalKey`, `lasthit`, `ip`) VALUES
('ac8dc9117bbfa832432939f24c2491a0', 2, 1524824714, '178.124.203.100');

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_categories`
--

CREATE TABLE `aw36_categories` (
  `id` int(11) NOT NULL,
  `category` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rank` int(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Categories to be used snippets,tv,chunks, etc';

--
-- Дамп данных таблицы `aw36_categories`
--

INSERT INTO `aw36_categories` (`id`, `category`, `rank`) VALUES
(1, 'SEO', 0),
(2, 'Js', 0),
(3, 'Templates', 0),
(4, 'Manager and Admin', 0),
(5, 'Content', 0),
(6, 'Navigation', 0),
(7, 'Forms', 0),
(8, 'add', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_documentgroup_names`
--

CREATE TABLE `aw36_documentgroup_names` (
  `id` int(10) NOT NULL,
  `name` varchar(245) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `private_memgroup` tinyint(4) DEFAULT '0' COMMENT 'determine whether the document group is private to manager users',
  `private_webgroup` tinyint(4) DEFAULT '0' COMMENT 'determines whether the document is private to web users'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_document_groups`
--

CREATE TABLE `aw36_document_groups` (
  `id` int(10) NOT NULL,
  `document_group` int(10) NOT NULL DEFAULT '0',
  `document` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_event_log`
--

CREATE TABLE `aw36_event_log` (
  `id` int(11) NOT NULL,
  `eventid` int(11) DEFAULT '0',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1- information, 2 - warning, 3- error',
  `user` int(11) NOT NULL DEFAULT '0' COMMENT 'link to user table',
  `usertype` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - manager, 1 - web',
  `source` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Stores event and error logs';

--
-- Дамп данных таблицы `aw36_event_log`
--

INSERT INTO `aw36_event_log` (`id`, `eventid`, `createdon`, `type`, `user`, `usertype`, `source`, `description`) VALUES
(1, 0, 1524507340, 3, 0, 1, 'Snippet - DocLister / Execution of a query to the ', '<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><h3 style=\"color:red\">Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'ASC\' at line 1</h3><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">SQL &gt; <span id=\"sqlHolder\">SELECT c.* FROM `desitionikaby`.`aw36_site_content` as `c`  WHERE c.parent IN (\'3\',\'9\') AND c.id NOT IN(\'3\',\'9\') AND c.deleted=0 AND c.published=1  ORDER BY ASC </span></div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>DocLister</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://des.itionika.by/tushenki.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[3] <a href=\"http://des.itionika.by/tushenki.html\" target=\"_blank\">Тушенки</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36 OPR/52.0.2871.64</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>91.149.189.9</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-04-23 21:15:40</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0,0019 s (7 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0,0427 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0,0446 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1,5133895874023 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>site_contentDocLister->getDocs</strong>()<br />assets/snippets/DocLister/snippet.DocLister.php on line 35</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>site_contentDocLister->getChildrenList</strong>()<br />assets/snippets/DocLister/core/controller/site_content.php on line 70</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocLister->dbQuery</strong>(string $var1)<br />assets/snippets/DocLister/core/controller/site_content.php on line 552</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DBAPI->query</strong>(string $var1)<br />assets/snippets/DocLister/core/DocLister.abstract.php on line 1801</td>\n	</tr>\n</table>\n'),
(2, 0, 1524676931, 3, 1, 0, 'Системные файлы были изменены.', 'Вы включили проверку системных файлов на наличие изменений, характерных для взломанных сайтов. Это не значит, что сайт был взломан, но желательно просмотреть измененные файлы.(index.php, .htaccess, manager/index.php, manager/includes/config.inc.php) index.php, .htaccess, manager/index.php, manager/includes/config.inc.php');

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_manager_log`
--

CREATE TABLE `aw36_manager_log` (
  `id` int(10) NOT NULL,
  `timestamp` int(20) NOT NULL DEFAULT '0',
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` int(10) NOT NULL DEFAULT '0',
  `itemid` varchar(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `itemname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains a record of user interaction.';

--
-- Дамп данных таблицы `aw36_manager_log`
--

INSERT INTO `aw36_manager_log` (`id`, `timestamp`, `internalKey`, `username`, `action`, `itemid`, `itemname`, `message`) VALUES
(1, 1522784472, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(2, 1522784473, 1, 'admin', 17, '-', '-', 'Editing settings'),
(3, 1522784479, 1, 'admin', 30, '-', '-', 'Saving settings'),
(4, 1522784483, 1, 'admin', 27, '1', 'Evolution CMS Install Success', 'Editing resource'),
(5, 1522784492, 1, 'admin', 5, '1', 'Главная', 'Saving resource'),
(6, 1522784492, 1, 'admin', 27, '1', 'Главная', 'Editing resource'),
(7, 1522784500, 1, 'admin', 5, '1', 'Главная', 'Saving resource'),
(8, 1522784500, 1, 'admin', 27, '1', 'Главная', 'Editing resource'),
(9, 1522784507, 1, 'admin', 76, '-', '-', 'Element management'),
(10, 1522784508, 1, 'admin', 16, '3', 'Minimal Template', 'Editing template'),
(11, 1522784519, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(12, 1522784519, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(13, 1522784586, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(14, 1522784586, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(15, 1522784625, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(16, 1522784625, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(17, 1522784634, 1, 'admin', 78, '2', 'header', 'Editing Chunk (HTML Snippet)'),
(18, 1522784685, 1, 'admin', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(19, 1522784685, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(20, 1522784688, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(21, 1522784688, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(22, 1522784712, 1, 'admin', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(23, 1522784712, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(24, 1522784728, 1, 'admin', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(25, 1522784728, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(26, 1522784773, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(27, 1522784773, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(28, 1522784799, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(29, 1522784799, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(30, 1522784811, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(31, 1522784811, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(32, 1522785135, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(33, 1522785135, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(34, 1522785194, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(35, 1522785194, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(36, 1522785205, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(37, 1522785205, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(38, 1522785226, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(39, 1522785227, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(40, 1522785235, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(41, 1522785235, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(42, 1522785382, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(43, 1522785382, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(44, 1522786732, 1, 'admin', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(45, 1522786732, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(46, 1522786746, 1, 'admin', 112, '2', 'Extras', 'Execute module'),
(47, 1522786822, 1, 'admin', 27, '1', 'Главная', 'Editing resource'),
(48, 1522786828, 1, 'admin', 76, '-', '-', 'Element management'),
(49, 1522786853, 1, 'admin', 78, '3', 'feedback', 'Editing Chunk (HTML Snippet)'),
(50, 1522786866, 1, 'admin', 76, '-', '-', 'Element management'),
(51, 1522786867, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(52, 1522786902, 1, 'admin', 78, '5', 'eFeedbackForm', 'Editing Chunk (HTML Snippet)'),
(53, 1522787002, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(54, 1522787002, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(55, 1522787024, 1, 'admin', 78, '4', 'eFeedbackReport', 'Editing Chunk (HTML Snippet)'),
(56, 1522787051, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(57, 1522787051, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(58, 1522787087, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(59, 1522787087, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(60, 1522787263, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(61, 1522787263, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(62, 1522865157, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(63, 1522865164, 1, 'admin', 76, '-', '-', 'Element management'),
(64, 1522865166, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(65, 1522865197, 1, 'admin', 76, '-', '-', 'Element management'),
(66, 1522865198, 1, 'admin', 78, '5', 'eFeedbackForm', 'Editing Chunk (HTML Snippet)'),
(67, 1522865276, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(68, 1522865276, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(69, 1522865280, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(70, 1522865282, 1, 'admin', 27, '1', 'Главная', 'Editing resource'),
(71, 1522865530, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(72, 1522865530, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(73, 1522865553, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(74, 1522865553, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(75, 1522865719, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(76, 1522865719, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(77, 1522865889, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(78, 1522865889, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(79, 1522866084, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(80, 1522866084, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(81, 1522866182, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(82, 1522866182, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(83, 1522866734, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(84, 1522866734, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(85, 1522866869, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(86, 1522866869, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(87, 1522867044, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(88, 1522867044, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(89, 1522867080, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(90, 1522867080, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(91, 1522867422, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(92, 1522867422, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(93, 1522867781, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(94, 1522868168, 1, 'admin', 76, '-', '-', 'Element management'),
(95, 1522868170, 1, 'admin', 301, '3', 'Meta description', 'Edit Template Variable'),
(96, 1522868170, 1, 'admin', 301, '2', 'Meta keywords', 'Edit Template Variable'),
(97, 1522868171, 1, 'admin', 301, '4', 'No index page', 'Edit Template Variable'),
(98, 1522868171, 1, 'admin', 301, '1', 'Meta title', 'Edit Template Variable'),
(99, 1522868177, 1, 'admin', 302, '3', 'Meta description', 'Save Template Variable'),
(100, 1522868177, 1, 'admin', 301, '3', 'Meta description', 'Edit Template Variable'),
(101, 1522868181, 1, 'admin', 302, '2', 'Meta keywords', 'Save Template Variable'),
(102, 1522868181, 1, 'admin', 301, '2', 'Meta keywords', 'Edit Template Variable'),
(103, 1522868184, 1, 'admin', 302, '4', 'No index page', 'Save Template Variable'),
(104, 1522868184, 1, 'admin', 301, '4', 'No index page', 'Edit Template Variable'),
(105, 1522868186, 1, 'admin', 302, '1', 'Meta title', 'Save Template Variable'),
(106, 1522868186, 1, 'admin', 301, '1', 'Meta title', 'Edit Template Variable'),
(107, 1522869371, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(108, 1522869385, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(109, 1522869399, 1, 'admin', 5, '-', 'load.js', 'Saving resource'),
(110, 1522869400, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(111, 1522869450, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(112, 1522869450, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(113, 1522869454, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(114, 1522869483, 1, 'admin', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(115, 1522869483, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(116, 1522869616, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(117, 1522869660, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(118, 1522869660, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(119, 1522869728, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(120, 1522869728, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(121, 1522869803, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(122, 1522869803, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(123, 1522869810, 1, 'admin', 27, '1', 'Главная', 'Editing resource'),
(124, 1522869814, 1, 'admin', 76, '-', '-', 'Element management'),
(125, 1522869816, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(126, 1522869838, 1, 'admin', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(127, 1522869838, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(128, 1522869845, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(129, 1522869845, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(130, 1522869906, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(131, 1522869906, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(132, 1522869921, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(133, 1522869921, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(134, 1522869941, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(135, 1522869941, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(136, 1522870067, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(137, 1522870067, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(138, 1522870158, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(139, 1522870158, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(140, 1522870471, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(141, 1522870471, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(142, 1523470371, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(143, 1523470493, 1, 'admin', 31, '-', '-', 'Using file manager'),
(144, 1523470497, 1, 'admin', 31, '-', '-', 'Using file manager'),
(145, 1523470498, 1, 'admin', 31, '-', '-', 'Using file manager'),
(146, 1523470499, 1, 'admin', 31, '-', '-', 'Using file manager'),
(147, 1523470502, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(148, 1523471281, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(149, 1523471281, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(150, 1523471450, 1, 'admin', 76, '-', '-', 'Element management'),
(151, 1523471452, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(152, 1523471477, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(153, 1523471477, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(154, 1523471796, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(155, 1523471796, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(156, 1523471833, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(157, 1523471833, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(158, 1523471902, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(159, 1523471902, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(160, 1523471944, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(161, 1523471944, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(162, 1523472091, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(163, 1523472091, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(164, 1523472293, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(165, 1523472293, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(166, 1523472467, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(167, 1523472468, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(168, 1523473567, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(169, 1523473630, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(170, 1523473630, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(171, 1523473738, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(172, 1523473738, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(173, 1523473796, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(174, 1523473796, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(175, 1523473823, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(176, 1523473823, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(177, 1523474113, 1, 'admin', 31, '-', '-', 'Using file manager'),
(178, 1523474117, 1, 'admin', 31, '-', '-', 'Using file manager'),
(179, 1523474119, 1, 'admin', 31, '-', '-', 'Using file manager'),
(180, 1523474122, 1, 'admin', 31, '-', '-', 'Using file manager'),
(181, 1523474124, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(182, 1523474146, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(183, 1523474146, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(184, 1523474415, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(185, 1523474415, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(186, 1523474520, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(187, 1523474520, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(188, 1523474528, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(189, 1523474528, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(190, 1523474771, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(191, 1523474771, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(192, 1523474797, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(193, 1523474797, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(194, 1523474876, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(195, 1523474876, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(196, 1523474901, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(197, 1523474901, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(198, 1523474976, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(199, 1523475000, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(200, 1523475000, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(201, 1523475032, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(202, 1523475032, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(203, 1523475156, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(204, 1523475156, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(205, 1523475359, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(206, 1523475359, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(207, 1523475692, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(208, 1523475692, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(209, 1523475843, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(210, 1523475843, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(211, 1523475949, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(212, 1523475949, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(213, 1523476000, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(214, 1523476000, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(215, 1523476035, 1, 'admin', 5, '2', 'load.js', 'Saving resource'),
(216, 1523476035, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(217, 1523643843, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(218, 1523643891, 1, 'admin', 31, '-', '-', 'Using file manager'),
(219, 1523643897, 1, 'admin', 31, '-', '-', 'Using file manager'),
(220, 1523643900, 1, 'admin', 31, '-', '-', 'Using file manager'),
(221, 1523643902, 1, 'admin', 31, '-', '-', 'Using file manager'),
(222, 1523643904, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(223, 1523643943, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(224, 1523643943, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(225, 1523644053, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(226, 1523644077, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(227, 1523644077, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(228, 1523644218, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(229, 1523644218, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(230, 1523644273, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(231, 1523644273, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(232, 1523644327, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(233, 1523644327, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(234, 1523644652, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(235, 1523644652, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(236, 1523644691, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(237, 1523644698, 1, 'admin', 31, '-', '-', 'Using file manager'),
(238, 1523644700, 1, 'admin', 31, '-', '-', 'Using file manager'),
(239, 1523644701, 1, 'admin', 31, '-', '-', 'Using file manager'),
(240, 1523644703, 1, 'admin', 31, '-', '-', 'Using file manager'),
(241, 1523644706, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/js/main.js', 'Viewing File'),
(242, 1523644720, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/js/main.js', 'Modified File'),
(243, 1523644720, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/js/main.js', 'Viewing File'),
(244, 1523644904, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(245, 1523644904, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(246, 1523644993, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(247, 1523644993, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(248, 1523740586, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(249, 1523740621, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(250, 1523740639, 1, 'admin', 5, '-', 'Продукция', 'Saving resource'),
(251, 1523740639, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(252, 1523740644, 1, 'admin', 76, '-', '-', 'Element management'),
(253, 1523740647, 1, 'admin', 96, '3', 'Главная Копия', 'Duplicate Template'),
(254, 1523740647, 1, 'admin', 16, '4', 'Главная Копия', 'Editing template'),
(255, 1523740729, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(256, 1523740729, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(257, 1523740751, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(258, 1523740751, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(259, 1523740756, 1, 'admin', 76, '-', '-', 'Element management'),
(260, 1523740759, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(261, 1523740761, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(262, 1523740762, 1, 'admin', 5, '3', 'Продукция', 'Saving resource'),
(263, 1523740763, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(264, 1523740798, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(265, 1523741202, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(266, 1523741202, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(267, 1523741331, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(268, 1523741331, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(269, 1523741354, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(270, 1523741354, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(271, 1523741401, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(272, 1523741401, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(273, 1523741409, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(274, 1523741409, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(275, 1523741432, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(276, 1523741432, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(277, 1523742360, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(278, 1523742372, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(279, 1523742390, 1, 'admin', 5, '-', 'style.css', 'Saving resource'),
(280, 1523742390, 1, 'admin', 27, '4', 'style.css', 'Editing resource'),
(281, 1523742396, 1, 'admin', 31, '-', '-', 'Using file manager'),
(282, 1523742397, 1, 'admin', 31, '-', '-', 'Using file manager'),
(283, 1523742400, 1, 'admin', 31, '-', '-', 'Using file manager'),
(284, 1523742402, 1, 'admin', 31, '-', '-', 'Using file manager'),
(285, 1523742405, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(286, 1523742415, 1, 'admin', 5, '4', 'style.css', 'Saving resource'),
(287, 1523742415, 1, 'admin', 27, '4', 'style.css', 'Editing resource'),
(288, 1523742431, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(289, 1523742446, 1, 'admin', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(290, 1523742446, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(291, 1523742465, 1, 'admin', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(292, 1523742465, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(293, 1523742468, 1, 'admin', 6, '4', 'style.css', 'Deleting resource'),
(294, 1523742468, 1, 'admin', 3, '4', 'style.css', 'Viewing data for resource'),
(295, 1523742531, 1, 'admin', 5, '4', 'style.css', 'Saving resource'),
(296, 1523742531, 1, 'admin', 27, '4', 'style.css', 'Editing resource'),
(297, 1523742547, 1, 'admin', 31, '-', '-', 'Using file manager'),
(298, 1523742549, 1, 'admin', 31, '-', '-', 'Using file manager'),
(299, 1523742550, 1, 'admin', 31, '-', '-', 'Using file manager'),
(300, 1523742552, 1, 'admin', 31, '-', '-', 'Using file manager'),
(301, 1523742555, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(302, 1523742565, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(303, 1523742565, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(304, 1523742594, 1, 'admin', 5, '3', 'Продукция', 'Saving resource'),
(305, 1523742594, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(306, 1523742614, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(307, 1523742614, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(308, 1523742619, 1, 'admin', 5, '3', 'Продукция', 'Saving resource'),
(309, 1523742619, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(310, 1523742714, 1, 'admin', 5, '3', 'Продукция', 'Saving resource'),
(311, 1523742714, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(312, 1523742783, 1, 'admin', 5, '3', 'Продукция', 'Saving resource'),
(313, 1523742783, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(314, 1523742809, 1, 'admin', 5, '3', 'Продукция', 'Saving resource'),
(315, 1523742809, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(316, 1523742862, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(317, 1523742862, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(318, 1523743694, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(319, 1523743694, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(320, 1523743712, 1, 'admin', 5, '3', 'Продукция', 'Saving resource'),
(321, 1523743712, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(322, 1523743722, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(323, 1523743722, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(324, 1523743799, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(325, 1523743799, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(326, 1523743847, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(327, 1523743847, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(328, 1523743883, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(329, 1523743883, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(330, 1523743906, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(331, 1523743906, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(332, 1523743957, 1, 'admin', 5, '3', 'Продукция', 'Saving resource'),
(333, 1523743957, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(334, 1524082259, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(335, 1524082273, 1, 'admin', 31, '-', '-', 'Using file manager'),
(336, 1524082285, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(337, 1524082358, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(338, 1524082418, 1, 'admin', 31, '-', '-', 'Using file manager'),
(339, 1524082444, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(340, 1524082447, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(341, 1524082460, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(342, 1524082547, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(343, 1524082547, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(344, 1524082586, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(345, 1524082586, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(346, 1524082644, 1, 'admin', 31, '-', '-', 'Using file manager'),
(347, 1524082645, 1, 'admin', 31, '-', '-', 'Using file manager'),
(348, 1524082646, 1, 'admin', 31, '-', '-', 'Using file manager'),
(349, 1524082649, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(350, 1524082863, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(351, 1524082863, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(352, 1524082910, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(353, 1524082910, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(354, 1524083075, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(355, 1524083075, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(356, 1524083113, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(357, 1524083113, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(358, 1524083139, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(359, 1524083139, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(360, 1524083170, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(361, 1524083170, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(362, 1524083217, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(363, 1524083217, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(364, 1524083275, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(365, 1524083275, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(366, 1524083303, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(367, 1524083303, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(368, 1524083327, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(369, 1524083327, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(370, 1524083342, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(371, 1524083342, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(372, 1524083371, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(373, 1524083371, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(374, 1524083384, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(375, 1524083384, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(376, 1524083418, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(377, 1524083418, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(378, 1524083434, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(379, 1524083434, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(380, 1524083446, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(381, 1524083446, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(382, 1524083530, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(383, 1524083530, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(384, 1524083548, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(385, 1524083548, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(386, 1524083670, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(387, 1524083670, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(388, 1524083717, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(389, 1524083717, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(390, 1524083724, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(391, 1524083724, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(392, 1524302238, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(393, 1524303812, 1, 'admin', 76, '-', '-', 'Element management'),
(394, 1524303813, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(395, 1524303829, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(396, 1524303829, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(397, 1524303868, 1, 'admin', 31, '-', '-', 'Using file manager'),
(398, 1524303870, 1, 'admin', 31, '-', '-', 'Using file manager'),
(399, 1524303871, 1, 'admin', 31, '-', '-', 'Using file manager'),
(400, 1524303873, 1, 'admin', 31, '-', '-', 'Using file manager'),
(401, 1524303876, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(402, 1524304015, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(403, 1524304015, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(404, 1524304023, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(405, 1524304023, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(406, 1524304140, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(407, 1524304140, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(408, 1524304248, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(409, 1524304248, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(410, 1524304283, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(411, 1524304283, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(412, 1524304310, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(413, 1524304310, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(414, 1524304340, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(415, 1524304341, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(416, 1524304408, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(417, 1524304408, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(418, 1524304461, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(419, 1524304461, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(420, 1524304645, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(421, 1524304645, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(422, 1524304681, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(423, 1524304681, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(424, 1524304720, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(425, 1524304720, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(426, 1524304761, 1, 'admin', 27, '2', 'load.js', 'Editing resource'),
(427, 1524304771, 1, 'admin', 31, '-', '-', 'Using file manager'),
(428, 1524304773, 1, 'admin', 31, '-', '-', 'Using file manager'),
(429, 1524304777, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/js/main.js', 'Viewing File'),
(430, 1524304787, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/js/main.js', 'Modified File'),
(431, 1524304787, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/js/main.js', 'Viewing File'),
(432, 1524305094, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(433, 1524305094, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(434, 1524305121, 1, 'admin', 27, '3', 'Продукция', 'Editing resource'),
(435, 1524305131, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(436, 1524305131, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(437, 1524305157, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(438, 1524305243, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(439, 1524305243, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(440, 1524305276, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(441, 1524305276, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(442, 1524305311, 1, 'admin', 20, '4', 'Внутренняя страница', 'Saving template'),
(443, 1524305311, 1, 'admin', 16, '4', 'Внутренняя страница', 'Editing template'),
(444, 1524305318, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(445, 1524305318, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(446, 1524305344, 1, 'admin', 20, '4', 'Продукция', 'Saving template'),
(447, 1524305344, 1, 'admin', 16, '4', 'Продукция', 'Editing template'),
(448, 1524305376, 1, 'admin', 77, '-', '-', 'Creating a new Chunk (HTML Snippet)'),
(449, 1524305379, 1, 'admin', 79, '-', 'footer', 'Saving Chunk (HTML Snippet)'),
(450, 1524305379, 1, 'admin', 78, '6', 'footer', 'Editing Chunk (HTML Snippet)'),
(451, 1524305384, 1, 'admin', 20, '4', 'Продукция', 'Saving template'),
(452, 1524305384, 1, 'admin', 16, '4', 'Продукция', 'Editing template'),
(453, 1524305402, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(454, 1524305402, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(455, 1524305451, 1, 'admin', 76, '-', '-', 'Element management'),
(456, 1524305453, 1, 'admin', 300, '-', 'Новый параметр (TV)', 'Create Template Variable'),
(457, 1524305485, 1, 'admin', 302, '-', 'Картинка под меню', 'Save Template Variable'),
(458, 1524305485, 1, 'admin', 301, '5', 'Картинка под меню', 'Edit Template Variable'),
(459, 1524305524, 1, 'admin', 302, '5', 'Картинка под меню', 'Save Template Variable'),
(460, 1524305524, 1, 'admin', 301, '5', 'Картинка под меню', 'Edit Template Variable'),
(461, 1524305545, 1, 'admin', 20, '4', 'Продукция', 'Saving template'),
(462, 1524305545, 1, 'admin', 16, '4', 'Продукция', 'Editing template'),
(463, 1524305550, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(464, 1524305564, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(465, 1524305564, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(466, 1524305575, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(467, 1524305575, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(468, 1524305581, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(469, 1524305581, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(470, 1524305593, 1, 'admin', 64, '-', '-', 'Removing deleted content'),
(471, 1524305642, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(472, 1524305663, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(473, 1524305697, 1, 'admin', 5, '-', 'Каши', 'Saving resource'),
(474, 1524305697, 1, 'admin', 27, '5', 'Каши', 'Editing resource'),
(475, 1524305707, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(476, 1524305726, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(477, 1524305727, 1, 'admin', 5, '-', 'Мясные закуски', 'Saving resource'),
(478, 1524305727, 1, 'admin', 27, '6', 'Мясные закуски', 'Editing resource'),
(479, 1524305744, 1, 'admin', 5, '6', 'Мясные закуски', 'Saving resource'),
(480, 1524305744, 1, 'admin', 27, '6', 'Мясные закуски', 'Editing resource'),
(481, 1524305759, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(482, 1524305766, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(483, 1524305788, 1, 'admin', 5, '-', 'Паштеты', 'Saving resource'),
(484, 1524305788, 1, 'admin', 27, '7', 'Паштеты', 'Editing resource'),
(485, 1524305803, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(486, 1524305868, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(487, 1524305868, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(488, 1524306055, 1, 'admin', 31, '-', '-', 'Using file manager'),
(489, 1524306056, 1, 'admin', 31, '-', '-', 'Using file manager'),
(490, 1524306057, 1, 'admin', 31, '-', '-', 'Using file manager'),
(491, 1524306058, 1, 'admin', 31, '-', '-', 'Using file manager'),
(492, 1524306060, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(493, 1524306069, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(494, 1524306069, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(495, 1524306136, 1, 'admin', 76, '-', '-', 'Element management'),
(496, 1524306138, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(497, 1524306148, 1, 'admin', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(498, 1524306148, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(499, 1524306216, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(500, 1524306216, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(501, 1524306310, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(502, 1524306310, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(503, 1524501642, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(504, 1524501646, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(505, 1524501982, 1, 'admin', 31, '-', '-', 'Using file manager'),
(506, 1524501984, 1, 'admin', 31, '-', '-', 'Using file manager'),
(507, 1524501985, 1, 'admin', 31, '-', '-', 'Using file manager'),
(508, 1524501986, 1, 'admin', 31, '-', '-', 'Using file manager'),
(509, 1524501989, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(510, 1524502133, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(511, 1524502133, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(512, 1524502223, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(513, 1524502231, 1, 'admin', 76, '-', '-', 'Element management'),
(514, 1524502317, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(515, 1524502398, 1, 'admin', 5, '-', 'Новый ресурс', 'Saving resource'),
(516, 1524502398, 1, 'admin', 27, '8', 'Новый ресурс', 'Editing resource'),
(517, 1524502410, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(518, 1524502423, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(519, 1524502427, 1, 'admin', 27, '8', 'Новый ресурс', 'Editing resource'),
(520, 1524502458, 1, 'admin', 5, '8', 'Тушеная говядина', 'Saving resource'),
(521, 1524502458, 1, 'admin', 27, '8', 'Тушеная говядина', 'Editing resource'),
(522, 1524502469, 1, 'admin', 16, '4', 'Продукция', 'Editing template'),
(523, 1524502472, 1, 'admin', 96, '4', 'Продукция Копия', 'Duplicate Template'),
(524, 1524502472, 1, 'admin', 16, '5', 'Продукция Копия', 'Editing template'),
(525, 1524502477, 1, 'admin', 20, '5', 'Продукт', 'Saving template'),
(526, 1524502477, 1, 'admin', 16, '5', 'Продукт', 'Editing template'),
(527, 1524502484, 1, 'admin', 27, '8', 'Тушеная говядина', 'Editing resource'),
(528, 1524502487, 1, 'admin', 27, '8', 'Тушеная говядина', 'Editing resource'),
(529, 1524502501, 1, 'admin', 76, '-', '-', 'Element management'),
(530, 1524502505, 1, 'admin', 300, '-', 'Новый параметр (TV)', 'Create Template Variable'),
(531, 1524506606, 1, 'admin', 302, '-', 'Картинка ппродукта', 'Save Template Variable'),
(532, 1524506606, 1, 'admin', 301, '6', 'Картинка ппродукта', 'Edit Template Variable'),
(533, 1524506642, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(534, 1524506648, 1, 'admin', 5, '-', 'Говядина', 'Saving resource'),
(535, 1524506649, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(536, 1524506656, 1, 'admin', 5, '9', 'Говядина', 'Saving resource'),
(537, 1524506656, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(538, 1524506661, 1, 'admin', 5, '-', 'Свинина', 'Saving resource'),
(539, 1524506661, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(540, 1524506672, 1, 'admin', 5, '-', 'Индейка', 'Saving resource'),
(541, 1524506672, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(542, 1524506675, 1, 'admin', 5, '-', 'Конина', 'Saving resource'),
(543, 1524506675, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(544, 1524506685, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(545, 1524506714, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(546, 1524506714, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(547, 1524506731, 1, 'admin', 76, '-', '-', 'Element management'),
(548, 1524506733, 1, 'admin', 300, '-', 'Новый параметр (TV)', 'Create Template Variable'),
(549, 1524506759, 1, 'admin', 302, '-', 'Картинка подкатегории', 'Save Template Variable'),
(550, 1524506759, 1, 'admin', 300, '-', 'Новый параметр (TV)', 'Create Template Variable'),
(551, 1524506760, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(552, 1524506786, 1, 'admin', 5, '9', 'Говядина', 'Saving resource'),
(553, 1524506786, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(554, 1524506787, 1, 'admin', 27, '10', 'Свинина', 'Editing resource'),
(555, 1524506857, 1, 'admin', 5, '10', 'Свинина', 'Saving resource'),
(556, 1524506857, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(557, 1524506859, 1, 'admin', 27, '11', 'Индейка', 'Editing resource'),
(558, 1524506893, 1, 'admin', 5, '11', 'Индейка', 'Saving resource'),
(559, 1524506893, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(560, 1524506894, 1, 'admin', 27, '12', 'Конина', 'Editing resource'),
(561, 1524506918, 1, 'admin', 5, '12', 'Конина', 'Saving resource'),
(562, 1524506918, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(563, 1524506919, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(564, 1524507037, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(565, 1524507037, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(566, 1524507046, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(567, 1524507048, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(568, 1524507092, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(569, 1524507093, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(570, 1524507096, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(571, 1524507102, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(572, 1524507102, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(573, 1524507149, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(574, 1524507149, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(575, 1524507186, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(576, 1524507186, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(577, 1524507191, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(578, 1524507191, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(579, 1524507197, 1, 'admin', 76, '-', '-', 'Element management'),
(580, 1524507199, 1, 'admin', 301, '7', 'Картинка подкатегории', 'Edit Template Variable'),
(581, 1524507205, 1, 'admin', 302, '7', 'Картинка подкатегории', 'Save Template Variable'),
(582, 1524507205, 1, 'admin', 301, '7', 'Картинка подкатегории', 'Edit Template Variable'),
(583, 1524507228, 1, 'admin', 302, '7', 'Картинка подкатегории', 'Save Template Variable'),
(584, 1524507228, 1, 'admin', 301, '7', 'Картинка подкатегории', 'Edit Template Variable'),
(585, 1524507298, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(586, 1524507298, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(587, 1524507338, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(588, 1524507338, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(589, 1524507352, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(590, 1524507352, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(591, 1524507374, 1, 'admin', 27, '11', 'Индейка', 'Editing resource'),
(592, 1524507401, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(593, 1524507401, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(594, 1524507419, 1, 'admin', 27, '10', 'Свинина', 'Editing resource'),
(595, 1524507425, 1, 'admin', 5, '10', 'Свинина', 'Saving resource'),
(596, 1524507425, 1, 'admin', 27, '10', 'Свинина', 'Editing resource'),
(597, 1524507448, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(598, 1524507448, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(599, 1524507494, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(600, 1524507494, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(601, 1524507539, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(602, 1524507539, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(603, 1524507545, 1, 'admin', 31, '-', '-', 'Using file manager'),
(604, 1524507547, 1, 'admin', 31, '-', '-', 'Using file manager'),
(605, 1524507548, 1, 'admin', 31, '-', '-', 'Using file manager'),
(606, 1524507549, 1, 'admin', 31, '-', '-', 'Using file manager'),
(607, 1524507552, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(608, 1524507632, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(609, 1524507632, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(610, 1524507646, 1, 'admin', 5, '3', 'Тушенки', 'Saving resource'),
(611, 1524507646, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(612, 1524507740, 1, 'admin', 27, '8', 'Тушеная говядина', 'Editing resource'),
(613, 1524507748, 1, 'admin', 27, '8', 'Тушеная говядина', 'Editing resource'),
(614, 1524507750, 1, 'admin', 5, '8', 'Тушеная говядина', 'Saving resource'),
(615, 1524507750, 1, 'admin', 27, '8', 'Тушеная говядина', 'Editing resource'),
(616, 1524507758, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(617, 1524507780, 1, 'admin', 76, '-', '-', 'Element management'),
(618, 1524507782, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(619, 1524507797, 1, 'admin', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(620, 1524507797, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(621, 1524507812, 1, 'admin', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(622, 1524507812, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(623, 1524511404, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(624, 1524511410, 1, 'admin', 27, '10', 'Свинина', 'Editing resource'),
(625, 1524511427, 1, 'admin', 5, '10', 'Свинина', 'Saving resource'),
(626, 1524511427, 1, 'admin', 27, '10', 'Свинина', 'Editing resource'),
(627, 1524511466, 1, 'admin', 31, '-', '-', 'Using file manager'),
(628, 1524511484, 1, 'admin', 31, '-', '-', 'Using file manager'),
(629, 1524511485, 1, 'admin', 31, '-', '-', 'Using file manager'),
(630, 1524511487, 1, 'admin', 31, '-', '-', 'Using file manager'),
(631, 1524511489, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(632, 1524511503, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(633, 1524511503, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(634, 1524511524, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(635, 1524511531, 1, 'admin', 27, '8', 'Тушеная говядина', 'Editing resource'),
(636, 1524511544, 1, 'admin', 76, '-', '-', 'Element management');
INSERT INTO `aw36_manager_log` (`id`, `timestamp`, `internalKey`, `username`, `action`, `itemid`, `itemname`, `message`) VALUES
(637, 1524511546, 1, 'admin', 16, '4', 'Продукция', 'Editing template'),
(638, 1524511549, 1, 'admin', 96, '4', 'Продукция Копия', 'Duplicate Template'),
(639, 1524511549, 1, 'admin', 16, '6', 'Продукция Копия', 'Editing template'),
(640, 1524511571, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(641, 1524511571, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(642, 1524511575, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(643, 1524511580, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(644, 1524511583, 1, 'admin', 5, '9', 'Говядина', 'Saving resource'),
(645, 1524511583, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(646, 1524511584, 1, 'admin', 27, '10', 'Свинина', 'Editing resource'),
(647, 1524511587, 1, 'admin', 27, '10', 'Свинина', 'Editing resource'),
(648, 1524511590, 1, 'admin', 27, '11', 'Индейка', 'Editing resource'),
(649, 1524511594, 1, 'admin', 27, '11', 'Индейка', 'Editing resource'),
(650, 1524511594, 1, 'admin', 5, '11', 'Индейка', 'Saving resource'),
(651, 1524511594, 1, 'admin', 27, '11', 'Индейка', 'Editing resource'),
(652, 1524511596, 1, 'admin', 5, '11', 'Индейка', 'Saving resource'),
(653, 1524511596, 1, 'admin', 27, '11', 'Индейка', 'Editing resource'),
(654, 1524511598, 1, 'admin', 27, '12', 'Конина', 'Editing resource'),
(655, 1524511600, 1, 'admin', 27, '12', 'Конина', 'Editing resource'),
(656, 1524511601, 1, 'admin', 5, '12', 'Конина', 'Saving resource'),
(657, 1524511601, 1, 'admin', 27, '12', 'Конина', 'Editing resource'),
(658, 1524511611, 1, 'admin', 27, '8', 'Тушеная говядина', 'Editing resource'),
(659, 1524511703, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(660, 1524511753, 1, 'admin', 112, '2', 'Extras', 'Execute module'),
(661, 1524511915, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(662, 1524511915, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(663, 1524512034, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(664, 1524512041, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(665, 1524512078, 1, 'admin', 5, '9', 'Говядина', 'Saving resource'),
(666, 1524512078, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(667, 1524512090, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(668, 1524512090, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(669, 1524512262, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(670, 1524512262, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(671, 1524512302, 1, 'admin', 5, '8', 'Тушеная говядина', 'Saving resource'),
(672, 1524512302, 1, 'admin', 27, '8', 'Тушеная говядина', 'Editing resource'),
(673, 1524512307, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(674, 1524512308, 1, 'admin', 5, '-', 'Новый ресурс', 'Saving resource'),
(675, 1524512308, 1, 'admin', 27, '13', 'Новый ресурс', 'Editing resource'),
(676, 1524512316, 1, 'admin', 27, '13', 'Новый ресурс', 'Editing resource'),
(677, 1524512337, 1, 'admin', 5, '13', 'Тушеная говядина', 'Saving resource'),
(678, 1524512337, 1, 'admin', 27, '13', 'Тушеная говядина', 'Editing resource'),
(679, 1524512344, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(680, 1524512354, 1, 'admin', 17, '-', '-', 'Editing settings'),
(681, 1524512359, 1, 'admin', 30, '-', '-', 'Saving settings'),
(682, 1524512361, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(683, 1524512365, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(684, 1524512389, 1, 'admin', 5, '-', 'Гуляш говяжий', 'Saving resource'),
(685, 1524512389, 1, 'admin', 27, '14', 'Гуляш говяжий', 'Editing resource'),
(686, 1524512393, 1, 'admin', 5, '14', 'Гуляш говяжий', 'Saving resource'),
(687, 1524512393, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(688, 1524512421, 1, 'admin', 5, '-', 'Говядина в белом соусе', 'Saving resource'),
(689, 1524512421, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(690, 1524512455, 1, 'admin', 5, '-', 'Говядина тушеная', 'Saving resource'),
(691, 1524512455, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(692, 1524512487, 1, 'admin', 5, '-', 'Завтрак в дорогу из говядины', 'Saving resource'),
(693, 1524512487, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(694, 1524512499, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(695, 1524512577, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(696, 1524512577, 1, 'admin', 19, '-', 'Новый шаблон', 'Creating a new template'),
(697, 1524512585, 1, 'admin', 76, '-', '-', 'Element management'),
(698, 1524512587, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(699, 1524512591, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(700, 1524512591, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(701, 1524512605, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(702, 1524512605, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(703, 1524512633, 1, 'admin', 27, '8', 'Тушеная говядина', 'Editing resource'),
(704, 1524512677, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(705, 1524512677, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(706, 1524512738, 1, 'admin', 31, '-', '-', 'Using file manager'),
(707, 1524512739, 1, 'admin', 31, '-', '-', 'Using file manager'),
(708, 1524512740, 1, 'admin', 31, '-', '-', 'Using file manager'),
(709, 1524512741, 1, 'admin', 31, '-', '-', 'Using file manager'),
(710, 1524512743, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(711, 1524512772, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(712, 1524512772, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(713, 1524512808, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(714, 1524512808, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(715, 1524512865, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(716, 1524512865, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(717, 1524512897, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(718, 1524512897, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(719, 1524513001, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(720, 1524513001, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(721, 1524513080, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(722, 1524513080, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(723, 1524513141, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(724, 1524513141, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(725, 1524513276, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(726, 1524513276, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(727, 1524513316, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(728, 1524513316, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(729, 1524513437, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(730, 1524513437, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(731, 1524513486, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(732, 1524513494, 1, 'admin', 20, '3', 'Главная', 'Saving template'),
(733, 1524513494, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(734, 1524513792, 1, 'admin', 31, '-', '-', 'Using file manager'),
(735, 1524513793, 1, 'admin', 31, '-', '-', 'Using file manager'),
(736, 1524513794, 1, 'admin', 31, '-', '-', 'Using file manager'),
(737, 1524513795, 1, 'admin', 31, '-', '-', 'Using file manager'),
(738, 1524513797, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(739, 1524513804, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Modified File'),
(740, 1524513804, 1, 'admin', 31, '-', '/h/desitionikaby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(741, 1524589939, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(742, 1524589978, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(743, 1524589999, 1, 'admin', 5, '9', 'Говядина', 'Saving resource'),
(744, 1524589999, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(745, 1524590017, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(746, 1524590022, 1, 'admin', 27, '5', 'Каши', 'Editing resource'),
(747, 1524590039, 1, 'admin', 5, '5', 'Каши', 'Saving resource'),
(748, 1524590039, 1, 'admin', 27, '5', 'Каши', 'Editing resource'),
(749, 1524674824, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(750, 1524674835, 1, 'admin', 93, '-', '-', 'Backup Manager'),
(751, 1524676931, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(752, 1524676938, 1, 'admin', 17, '-', '-', 'Editing settings'),
(753, 1524676945, 1, 'admin', 30, '-', '-', 'Saving settings'),
(754, 1524692026, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(755, 1524692058, 1, 'admin', 27, '11', 'Индейка', 'Editing resource'),
(756, 1524692087, 1, 'admin', 5, '11', 'Индейка', 'Saving resource'),
(757, 1524692087, 1, 'admin', 27, '11', 'Индейка', 'Editing resource'),
(758, 1524692089, 1, 'admin', 27, '12', 'Конина', 'Editing resource'),
(759, 1524692110, 1, 'admin', 5, '12', 'Конина', 'Saving resource'),
(760, 1524692110, 1, 'admin', 27, '12', 'Конина', 'Editing resource'),
(761, 1524692113, 1, 'admin', 27, '10', 'Свинина', 'Editing resource'),
(762, 1524692132, 1, 'admin', 27, '3', 'Тушенки', 'Editing resource'),
(763, 1524692133, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(764, 1524692145, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(765, 1524692206, 1, 'admin', 5, '-', 'Цыпленок', 'Saving resource'),
(766, 1524692206, 1, 'admin', 27, '18', 'Цыпленок', 'Editing resource'),
(767, 1524692215, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(768, 1524692308, 1, 'admin', 5, '18', 'Цыпленок', 'Saving resource'),
(769, 1524692308, 1, 'admin', 27, '18', 'Цыпленок', 'Editing resource'),
(770, 1524692462, 1, 'admin', 16, '4', 'Продукция', 'Editing template'),
(771, 1524692474, 1, 'admin', 20, '4', 'Продукция', 'Saving template'),
(772, 1524692475, 1, 'admin', 16, '4', 'Продукция', 'Editing template'),
(773, 1524692656, 1, 'admin', 31, '-', '-', 'Using file manager'),
(774, 1524692657, 1, 'admin', 31, '-', '-', 'Using file manager'),
(775, 1524692659, 1, 'admin', 31, '-', '-', 'Using file manager'),
(776, 1524692660, 1, 'admin', 31, '-', '-', 'Using file manager'),
(777, 1524692662, 1, 'admin', 31, '-', '/h/miaskonby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(778, 1524692917, 1, 'admin', 31, '-', '/h/miaskonby/htdocs/assets/templates/css/style.css', 'Modified File'),
(779, 1524692917, 1, 'admin', 31, '-', '/h/miaskonby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(780, 1524725773, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(781, 1524725782, 1, 'admin', 11, '-', 'Новый пользователь', 'Creating a user'),
(782, 1524725826, 1, 'admin', 32, '-', 'des', 'Saving user'),
(783, 1524725833, 1, 'admin', 8, '-', '-', 'Logged out'),
(784, 1524725839, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(785, 1524725844, 2, 'des', 31, '-', '-', 'Using file manager'),
(786, 1524725855, 2, 'des', 8, '-', '-', 'Logged out'),
(787, 1524725867, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(788, 1524725878, 1, 'admin', 40, '-', '-', 'Editing Access Permissions'),
(789, 1524725897, 1, 'admin', 12, '2', 'des', 'Editing user'),
(790, 1524725917, 1, 'admin', 86, '-', '-', 'Role management'),
(791, 1524725919, 1, 'admin', 35, '3', 'Publisher', 'Editing role'),
(792, 1524725992, 1, 'admin', 36, '3', 'Publisher', 'Saving role'),
(793, 1524725992, 1, 'admin', 86, '-', '-', 'Role management'),
(794, 1524726001, 1, 'admin', 8, '-', '-', 'Logged out'),
(795, 1524726004, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(796, 1524726012, 2, 'des', 99, '-', '-', 'Manage Web Users'),
(797, 1524726027, 2, 'des', 8, '-', '-', 'Logged out'),
(798, 1524726042, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(799, 1524726051, 1, 'admin', 17, '-', '-', 'Editing settings'),
(800, 1524726066, 1, 'admin', 17, '-', '-', 'Editing settings'),
(801, 1524726084, 1, 'admin', 30, '-', '-', 'Saving settings'),
(802, 1524727332, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(803, 1524727356, 2, 'des', 27, '1', 'Главная', 'Editing resource'),
(804, 1524727364, 2, 'des', 27, '3', 'Тушенки', 'Editing resource'),
(805, 1524727377, 2, 'des', 27, '5', 'Каши', 'Editing resource'),
(806, 1524727609, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(807, 1524732425, 2, 'des', 27, '1', 'Главная', 'Editing resource'),
(808, 1524736997, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(809, 1524737071, 2, 'des', 27, '1', 'Главная', 'Editing resource'),
(810, 1524746189, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(811, 1524746271, 2, 'des', 112, '1', 'Doc Manager', 'Execute module'),
(812, 1524746276, 2, 'des', 112, '2', 'Extras', 'Execute module'),
(813, 1524746285, 2, 'des', 99, '-', '-', 'Manage Web Users'),
(814, 1524746289, 2, 'des', 27, '1', 'Главная', 'Editing resource'),
(815, 1524746345, 2, 'des', 27, '1', 'Главная', 'Editing resource'),
(816, 1524747633, 2, 'des', 9, '-', '-', 'Viewing help'),
(817, 1524747636, 2, 'des', 112, '2', 'Extras', 'Execute module'),
(818, 1524747639, 2, 'des', 99, '-', '-', 'Manage Web Users'),
(819, 1524747642, 2, 'des', 87, '-', 'Новый веб-пользователь', 'Create new web user'),
(820, 1524747861, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(821, 1524747939, 2, 'des', 27, '5', 'Каши', 'Editing resource'),
(822, 1524748593, 2, 'des', 26, '-', '-', 'Refreshing site'),
(823, 1524748599, 2, 'des', 26, '-', '-', 'Refreshing site'),
(824, 1524749164, 2, 'des', 53, '-', '-', 'Viewing system info'),
(825, 1524756448, 2, 'des', 27, '1', 'Главная', 'Editing resource'),
(826, 1524762202, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(827, 1524762209, 1, 'admin', 17, '-', '-', 'Editing settings'),
(828, 1524762213, 1, 'admin', 40, '-', '-', 'Editing Access Permissions'),
(829, 1524762216, 1, 'admin', 91, '-', '-', 'Editing Web Access Permissions'),
(830, 1524762217, 1, 'admin', 86, '-', '-', 'Role management'),
(831, 1524762220, 1, 'admin', 35, '2', 'Editor', 'Editing role'),
(832, 1524762257, 1, 'admin', 86, '-', '-', 'Role management'),
(833, 1524762258, 1, 'admin', 35, '3', 'Publisher', 'Editing role'),
(834, 1524762268, 1, 'admin', 12, '2', 'des', 'Editing user'),
(835, 1524762324, 1, 'admin', 36, '3', 'Publisher', 'Saving role'),
(836, 1524762324, 1, 'admin', 86, '-', '-', 'Role management'),
(837, 1524762506, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(838, 1524762514, 2, 'des', 76, '-', '-', 'Element management'),
(839, 1524762518, 2, 'des', 16, '3', 'Главная', 'Editing template'),
(840, 1524762576, 1, 'admin', 86, '-', '-', 'Role management'),
(841, 1524765262, 2, 'des', 20, '3', 'Главная', 'Saving template'),
(842, 1524765262, 2, 'des', 16, '3', 'Главная', 'Editing template'),
(843, 1524765294, 2, 'des', 16, '5', 'Продукт', 'Editing template'),
(844, 1524765294, 2, 'des', 16, '4', 'Продукция', 'Editing template'),
(845, 1524765294, 2, 'des', 16, '6', 'Продукция (родитель)', 'Editing template'),
(846, 1524765306, 2, 'des', 20, '5', 'Продукт', 'Saving template'),
(847, 1524765306, 2, 'des', 16, '5', 'Продукт', 'Editing template'),
(848, 1524765309, 2, 'des', 20, '4', 'Продукция', 'Saving template'),
(849, 1524765309, 2, 'des', 16, '4', 'Продукция', 'Editing template'),
(850, 1524765313, 2, 'des', 20, '6', 'Продукция (родитель)', 'Saving template'),
(851, 1524765313, 2, 'des', 16, '6', 'Продукция (родитель)', 'Editing template'),
(852, 1524765336, 2, 'des', 76, '-', '-', 'Element management'),
(853, 1524765339, 2, 'des', 16, '5', 'Продукт', 'Editing template'),
(854, 1524765348, 2, 'des', 27, '1', 'Главная', 'Editing resource'),
(855, 1524765557, 2, 'des', 106, '-', '-', 'Viewing Modules'),
(856, 1524765568, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(857, 1524765908, 2, 'des', 26, '-', '-', 'Refreshing site'),
(858, 1524765916, 2, 'des', 26, '-', '-', 'Refreshing site'),
(859, 1524766154, 2, 'des', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(860, 1524766154, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(861, 1524766180, 2, 'des', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(862, 1524766180, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(863, 1524766370, 2, 'des', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(864, 1524766370, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(865, 1524767034, 2, 'des', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(866, 1524767034, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(867, 1524767117, 2, 'des', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(868, 1524767117, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(869, 1524767686, 2, 'des', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(870, 1524767686, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(871, 1524767691, 2, 'des', 53, '-', '-', 'Viewing system info'),
(872, 1524767704, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(873, 1524767865, 2, 'des', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(874, 1524767865, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(875, 1524767880, 2, 'des', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(876, 1524767880, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(877, 1524769775, 2, 'des', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(878, 1524769775, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(879, 1524771229, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(880, 1524771234, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(881, 1524771242, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(882, 1524771272, 1, 'admin', 5, '-', 'style.css', 'Saving resource'),
(883, 1524771272, 1, 'admin', 27, '19', 'style.css', 'Editing resource'),
(884, 1524771278, 1, 'admin', 16, '3', 'Главная', 'Editing template'),
(885, 1524771282, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(886, 1524771297, 1, 'admin', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(887, 1524771297, 1, 'admin', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(888, 1524771317, 1, 'admin', 31, '-', '-', 'Using file manager'),
(889, 1524771318, 1, 'admin', 31, '-', '-', 'Using file manager'),
(890, 1524771320, 1, 'admin', 31, '-', '-', 'Using file manager'),
(891, 1524771321, 1, 'admin', 31, '-', '-', 'Using file manager'),
(892, 1524771323, 1, 'admin', 31, '-', '/h/miaskonby/htdocs/assets/templates/css/style.css', 'Viewing File'),
(893, 1524771336, 1, 'admin', 5, '19', 'style.css', 'Saving resource'),
(894, 1524771336, 1, 'admin', 27, '19', 'style.css', 'Editing resource'),
(895, 1524771369, 1, 'admin', 5, '19', 'style.css', 'Saving resource'),
(896, 1524771369, 1, 'admin', 27, '19', 'style.css', 'Editing resource'),
(897, 1524771410, 1, 'admin', 5, '19', 'style.css', 'Saving resource'),
(898, 1524771411, 1, 'admin', 27, '19', 'style.css', 'Editing resource'),
(899, 1524771503, 1, 'admin', 8, '-', '-', 'Logged out'),
(900, 1524771517, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(901, 1524771521, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(902, 1524771524, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(903, 1524771524, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(904, 1524775377, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(905, 1524775379, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(906, 1524775399, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(907, 1524775399, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(908, 1524775657, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(909, 1524775657, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(910, 1524775673, 2, 'des', 26, '-', '-', 'Refreshing site'),
(911, 1524775680, 2, 'des', 26, '-', '-', 'Refreshing site'),
(912, 1524775705, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(913, 1524775705, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(914, 1524775727, 2, 'des', 26, '-', '-', 'Refreshing site'),
(915, 1524775751, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(916, 1524775751, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(917, 1524775786, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(918, 1524775786, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(919, 1524775835, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(920, 1524775835, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(921, 1524775859, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(922, 1524775859, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(923, 1524775885, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(924, 1524775885, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(925, 1524776296, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(926, 1524813521, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(927, 1524817299, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(928, 1524817317, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(929, 1524817429, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(930, 1524817429, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(931, 1524817477, 2, 'des', 16, '3', 'Главная', 'Editing template'),
(932, 1524817783, 2, 'des', 20, '3', 'Главная', 'Saving template'),
(933, 1524817783, 2, 'des', 16, '3', 'Главная', 'Editing template'),
(934, 1524817843, 2, 'des', 20, '3', 'Главная', 'Saving template'),
(935, 1524817843, 2, 'des', 16, '3', 'Главная', 'Editing template'),
(936, 1524817867, 2, 'des', 20, '3', 'Главная', 'Saving template'),
(937, 1524817867, 2, 'des', 16, '3', 'Главная', 'Editing template'),
(938, 1524817912, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(939, 1524817912, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(940, 1524818696, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(941, 1524818696, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(942, 1524818764, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(943, 1524818764, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(944, 1524819155, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(945, 1524819155, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(946, 1524819194, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(947, 1524819194, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(948, 1524819322, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(949, 1524819323, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(950, 1524819983, 2, 'des', 27, '8', 'Тушеная говядина', 'Editing resource'),
(951, 1524820035, 2, 'des', 16, '5', 'Продукт', 'Editing template'),
(952, 1524820084, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(953, 1524820193, 2, 'des', 79, '2', 'head', 'Saving Chunk (HTML Snippet)'),
(954, 1524820193, 2, 'des', 78, '2', 'head', 'Editing Chunk (HTML Snippet)'),
(955, 1524820529, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(956, 1524820891, 1, 'admin', 67, '-', '-', 'Removing locks'),
(957, 1524820892, 1, 'admin', 27, '19', 'style.css', 'Editing resource'),
(958, 1524820955, 1, 'admin', 5, '19', 'style.css', 'Saving resource'),
(959, 1524820955, 1, 'admin', 27, '19', 'style.css', 'Editing resource'),
(960, 1524820983, 1, 'admin', 27, '9', 'Говядина', 'Editing resource'),
(961, 1524820993, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(962, 1524821010, 1, 'admin', 20, '6', 'Продукция (родитель)', 'Saving template'),
(963, 1524821010, 1, 'admin', 16, '6', 'Продукция (родитель)', 'Editing template'),
(964, 1524821026, 1, 'admin', 5, '19', 'style.css', 'Saving resource'),
(965, 1524821026, 1, 'admin', 27, '19', 'style.css', 'Editing resource'),
(966, 1524821067, 1, 'admin', 5, '19', 'style.css', 'Saving resource'),
(967, 1524821067, 1, 'admin', 27, '19', 'style.css', 'Editing resource'),
(968, 1524821100, 1, 'admin', 5, '19', 'style.css', 'Saving resource'),
(969, 1524821100, 1, 'admin', 27, '19', 'style.css', 'Editing resource'),
(970, 1524821134, 1, 'admin', 5, '19', 'style.css', 'Saving resource'),
(971, 1524821134, 1, 'admin', 27, '19', 'style.css', 'Editing resource'),
(972, 1524821164, 1, 'admin', 5, '19', 'style.css', 'Saving resource'),
(973, 1524821164, 1, 'admin', 27, '19', 'style.css', 'Editing resource'),
(974, 1524821203, 1, 'admin', 8, '-', '-', 'Logged out'),
(975, 1524821207, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(976, 1524821211, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(977, 1524821221, 2, 'des', 5, '19', 'style.css', 'Saving resource'),
(978, 1524821221, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(979, 1524822694, 2, 'des', 8, '-', '-', 'Logged out'),
(980, 1524822929, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(981, 1524822933, 2, 'des', 53, '-', '-', 'Viewing system info'),
(982, 1524823038, 2, 'des', 8, '-', '-', 'Logged out'),
(983, 1524823834, 2, 'des', 58, '-', 'MODX', 'Logged in'),
(984, 1524823842, 2, 'des', 28, '-', '-', 'Changing password'),
(985, 1524823847, 2, 'des', 114, '-', '-', 'View event log'),
(986, 1524824577, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(987, 1524824595, 2, 'des', 27, '19', 'style.css', 'Editing resource'),
(988, 1524824648, 2, 'des', 16, '3', 'Главная', 'Editing template');

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_manager_users`
--

CREATE TABLE `aw36_manager_users` (
  `id` int(10) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains login information for backend users.';

--
-- Дамп данных таблицы `aw36_manager_users`
--

INSERT INTO `aw36_manager_users` (`id`, `username`, `password`) VALUES
(1, 'admin', '$P$Bs8F9rbm3cZegp.j.MklznqyLeFNtC/'),
(2, 'des', '$P$BBU0bUopclAHdwq9RH7OOAeEhGidAm0');

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_membergroup_access`
--

CREATE TABLE `aw36_membergroup_access` (
  `id` int(10) NOT NULL,
  `membergroup` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_membergroup_names`
--

CREATE TABLE `aw36_membergroup_names` (
  `id` int(10) NOT NULL,
  `name` varchar(245) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_member_groups`
--

CREATE TABLE `aw36_member_groups` (
  `id` int(10) NOT NULL,
  `user_group` int(10) NOT NULL DEFAULT '0',
  `member` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_content`
--

CREATE TABLE `aw36_site_content` (
  `id` int(10) NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'document',
  `contentType` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text/html',
  `pagetitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `longtitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(245) COLLATE utf8_unicode_ci DEFAULT '',
  `link_attributes` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Link attriubtes',
  `published` int(1) NOT NULL DEFAULT '0',
  `pub_date` int(20) NOT NULL DEFAULT '0',
  `unpub_date` int(20) NOT NULL DEFAULT '0',
  `parent` int(10) NOT NULL DEFAULT '0',
  `isfolder` int(1) NOT NULL DEFAULT '0',
  `introtext` text COLLATE utf8_unicode_ci COMMENT 'Used to provide quick summary of the document',
  `content` mediumtext COLLATE utf8_unicode_ci,
  `richtext` tinyint(1) NOT NULL DEFAULT '1',
  `template` int(10) NOT NULL DEFAULT '0',
  `menuindex` int(10) NOT NULL DEFAULT '0',
  `searchable` int(1) NOT NULL DEFAULT '1',
  `cacheable` int(1) NOT NULL DEFAULT '1',
  `createdby` int(10) NOT NULL DEFAULT '0',
  `createdon` int(20) NOT NULL DEFAULT '0',
  `editedby` int(10) NOT NULL DEFAULT '0',
  `editedon` int(20) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `deletedon` int(20) NOT NULL DEFAULT '0',
  `deletedby` int(10) NOT NULL DEFAULT '0',
  `publishedon` int(20) NOT NULL DEFAULT '0' COMMENT 'Date the document was published',
  `publishedby` int(10) NOT NULL DEFAULT '0' COMMENT 'ID of user who published the document',
  `menutitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Menu title',
  `donthit` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Disable page hit count',
  `privateweb` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Private web document',
  `privatemgr` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Private manager document',
  `content_dispo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-inline, 1-attachment',
  `hidemenu` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Hide document from menu',
  `alias_visible` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains the site document tree.';

--
-- Дамп данных таблицы `aw36_site_content`
--

INSERT INTO `aw36_site_content` (`id`, `type`, `contentType`, `pagetitle`, `longtitle`, `description`, `alias`, `link_attributes`, `published`, `pub_date`, `unpub_date`, `parent`, `isfolder`, `introtext`, `content`, `richtext`, `template`, `menuindex`, `searchable`, `cacheable`, `createdby`, `createdon`, `editedby`, `editedon`, `deleted`, `deletedon`, `deletedby`, `publishedon`, `publishedby`, `menutitle`, `donthit`, `privateweb`, `privatemgr`, `content_dispo`, `hidemenu`, `alias_visible`) VALUES
(1, 'document', 'text/html', 'Главная', '', '', 'index', '', 1, 0, 0, 0, 0, '', '', 1, 3, 0, 1, 1, 1, 1130304721, 1, 1522784500, 0, 0, 0, 1130304721, 1, 'Base Install', 0, 0, 0, 0, 0, 1),
(2, 'document', 'text/javascript', 'load.js', '', '', 'load.js', '', 1, 0, 0, 0, 0, '', '$(function() {\n	if($(document).scrollTop() < 100){\n			$(\'.up\').hide();\n	}\n	if($(window).width() <= \'764\')\n		$(\'head\').append(\'<script src=\"assets\\\\templates\\\\js\\\\main.js\"></script>\');\n	else{\n		$(\'head\').append(\'<script src=\"assets\\\\templates\\\\js\\\\wow.js\"></script><script src=\"assets\\\\templates\\\\js\\\\main.js\"></script>\');\n		new WOW().init();\n	}\n	$(\'.up\').click(function() {\n    $(\'html, body\').animate({scrollTop: 0},1000);\n    return false;\n	})\n	$(document).scroll(function(e){\n		if($(document).scrollTop() < 100){\n			$(\'.up\').hide(300);\n		}\n		else{\n			$(\'.up\').show(300);\n		}\n});\n});\n\n', 0, 0, 1, 0, 1, 1, 1522869399, 1, 1523476035, 0, 0, 0, 1522869399, 1, '', 1, 0, 0, 0, 1, 1),
(3, 'document', 'text/html', 'Тушенки', '', '', 'tushenki', '', 1, 0, 0, 0, 1, 'Неослабевающий спрос на консервы ТМ Гродфуд обеспечен их стабильным и высоким качеством — все они изготавливаются\n			строго по ГОСТу, а интерес к ним на рынке постоянно «подогревается» новыми акцентами-находками маркетинговой\n			службы компании, которые непременно становятся новыми «стандартами» для всех производителей мясной консервации.\n			На сегодняшний день это уже и обязательное использование банки с ключом easy-open и акцентированное обозначение\n			на упаковке содержания мяса в 97,5%.', '<div class=\"row\">\n	[!DocLister\n	&depth=`0`\n	&tpl=`@CODE:\n	<div class=\"prod\">\n		<div class=\"product-image\">\n			<a href=\"[+url+]\">\n				<img src=\"[+tv.img_cat+]\" alt=\"[+pagetitle+]\">\n			</a>\n		</div>\n		<div class=\"product-text\">\n			<a href=\"[+url+]\">[+pagetitle+]</a>\n			<p>Неослабевающий спрос на консервы ТМ Гродфуд обеспечен их стабильным и высоким качеством — все они изготавливаются\n				строго по ГОСТу, а интерес к ним на рынке постоянно «подогревается» новыми акцентами-находками маркетинговой\n				службы компании, которые непременно становятся новыми «стандартами» для всех производителей мясной консервации.\n				На сегодняшний день это уже и обязательное использование банки с ключом easy-open и акцентированное обозначение\n				на упаковке содержания мяса в 97,5%. </p>\n		</div>\n	</div>`\n	&tvList=`img_cat`\n	&order=`ASC`\n	!]\n</div>', 0, 4, 2, 1, 1, 1, 1523740639, 1, 1524507645, 0, 0, 0, 1523740639, 1, '', 0, 0, 0, 0, 0, 1),
(5, 'document', 'text/html', 'Каши', '', '', 'kashi', '', 1, 0, 0, 0, 0, 'Неослабевающий спрос на консервы ТМ Гродфуд обеспечен их стабильным и высоким качеством — все они изготавливаются строго по ГОСТу, а интерес к ним на рынке постоянно «подогревается» новыми акцентами-находками маркетинговой службы компании, которые непременно становятся новыми «стандартами» для всех производителей мясной консервации. На сегодняшний день это уже и обязательное использование банки с ключом easy-open и акцентированное обозначение на упаковке содержания мяса в 97,5%.', '', 1, 4, 3, 1, 1, 1, 1524305697, 1, 1524590039, 0, 0, 0, 1524305697, 1, '', 0, 0, 0, 0, 0, 1),
(6, 'document', 'text/html', 'Мясные закуски', '', '', 'myasnye-zakuski', '', 1, 0, 0, 0, 0, 'Неослабевающий спрос на консервы ТМ Гродфуд обеспечен их стабильным и высоким качеством — все они изготавливаются строго по ГОСТу, а интерес к ним на рынке постоянно «подогревается» новыми акцентами-находками маркетинговой службы компании, которые непременно становятся новыми «стандартами» для всех производителей мясной консервации. На сегодняшний день это уже и обязательное использование банки с ключом easy-open и акцентированное обозначение на упаковке содержания мяса в 97,5%.', '', 1, 4, 4, 1, 1, 1, 1524305727, 1, 1524305744, 0, 0, 0, 1524305727, 1, '', 0, 0, 0, 0, 0, 1),
(7, 'document', 'text/html', 'Паштеты', '', '', 'pashtety', '', 1, 0, 0, 0, 0, 'Неослабевающий спрос на консервы ТМ Гродфуд обеспечен их стабильным и высоким качеством — все они изготавливаются строго по ГОСТу, а интерес к ним на рынке постоянно «подогревается» новыми акцентами-находками маркетинговой службы компании, которые непременно становятся новыми «стандартами» для всех производителей мясной консервации. На сегодняшний день это уже и обязательное использование банки с ключом easy-open и акцентированное обозначение на упаковке содержания мяса в 97,5%.', '', 1, 4, 5, 1, 1, 1, 1524305788, 1, 1524305788, 0, 0, 0, 1524305788, 1, '', 0, 0, 0, 0, 0, 1),
(8, 'document', 'text/html', 'Тушеная говядина', '', '', 'tushenaya-govyadina', '', 1, 0, 0, 9, 0, '', '', 0, 5, 0, 1, 1, 1, 1524502398, 1, 1524512302, 0, 0, 0, 1524502398, 1, '', 0, 0, 0, 0, 0, 1),
(9, 'document', 'text/html', 'Говядина', '', '', 'govyadina', '', 1, 0, 0, 3, 1, '', '', 1, 6, 0, 1, 1, 1, 1524506648, 1, 1524589999, 0, 0, 0, 1524506648, 1, '', 0, 0, 0, 0, 0, 1),
(10, 'document', 'text/html', 'Свинина', '', '', 'svinina', '', 1, 0, 0, 3, 0, '', '', 1, 4, 1, 1, 1, 1, 1524506661, 1, 1524511427, 0, 0, 0, 1524506661, 1, '', 0, 0, 0, 0, 0, 1),
(11, 'document', 'text/html', 'Индейка', '', '', 'indejka', '', 1, 0, 0, 3, 0, '', '', 1, 6, 2, 1, 1, 1, 1524506672, 1, 1524692087, 0, 0, 0, 1524506672, 1, '', 0, 0, 0, 0, 0, 1),
(12, 'document', 'text/html', 'Конина', '', '', 'konina', '', 1, 0, 0, 3, 0, '', '', 1, 6, 3, 1, 1, 1, 1524506675, 1, 1524692110, 0, 0, 0, 1524506675, 1, '', 0, 0, 0, 0, 0, 1),
(13, 'document', 'text/html', 'Тушеная говядина', '', '', 'tushenaya-govyadina1', '', 1, 0, 0, 9, 0, '', '', 1, 5, 1, 1, 1, 1, 1524512308, 1, 1524512337, 0, 0, 0, 1524512308, 1, '', 0, 0, 0, 0, 0, 1),
(14, 'document', 'text/html', 'Гуляш говяжий', '', '', 'gulyash-govyazhij', '', 1, 0, 0, 9, 0, '', '', 1, 5, 2, 1, 1, 1, 1524512389, 1, 1524512393, 0, 0, 0, 1524512389, 1, '', 0, 0, 0, 0, 0, 1),
(15, 'document', 'text/html', 'Говядина в белом соусе', '', '', 'govyadina-v-belom-souse', '', 1, 0, 0, 9, 0, '', '', 1, 5, 3, 1, 1, 1, 1524512421, 1, 1524512421, 0, 0, 0, 1524512421, 1, '', 0, 0, 0, 0, 0, 1),
(16, 'document', 'text/html', 'Говядина тушеная', '', '', 'govyadina-tushenaya', '', 1, 0, 0, 9, 0, '', '', 1, 5, 4, 1, 1, 1, 1524512455, 1, 1524512455, 0, 0, 0, 1524512455, 1, '', 0, 0, 0, 0, 0, 1),
(17, 'document', 'text/html', 'Завтрак в дорогу из говядины', '', '', 'zavtrak-v-dorogu-iz-govyadiny', '', 1, 0, 0, 9, 0, '', '', 1, 5, 5, 1, 1, 1, 1524512487, 1, 1524512487, 0, 0, 0, 1524512487, 1, '', 0, 0, 0, 0, 0, 1),
(18, 'document', 'text/html', 'Цыпленок', '', '', 'cyplenok', '', 1, 0, 0, 3, 0, '', '', 1, 4, 4, 1, 1, 1, 1524692206, 1, 1524692308, 0, 0, 0, 1524692206, 1, '', 0, 0, 0, 0, 0, 1),
(19, 'document', 'text/css', 'style.css', '', '', 'style.css', '', 1, 0, 0, 0, 0, '', ':active, :hover, :focus {\r\n    outline: 0;\r\n    outline-offset: 0;\r\n}\r\n/* Slider */\r\n*{\r\n	/*line-height: 1;*/\r\n	font-family: \'Montserrat\', sans-serif;\r\n}\r\n.slick-slider\r\n{\r\n	position: relative;\r\n\r\n	display: block;\r\n	box-sizing: border-box;\r\n\r\n	-webkit-user-select: none;\r\n	-moz-user-select: none;\r\n	-ms-user-select: none;\r\n	user-select: none;\r\n\r\n	-webkit-touch-callout: none;\r\n	-khtml-user-select: none;\r\n	-ms-touch-action: pan-y;\r\n	touch-action: pan-y;\r\n	-webkit-tap-highlight-color: transparent;\r\n}\r\n\r\n.slick-list\r\n{\r\n	position: relative;\r\n\r\n	display: block;\r\n	overflow: hidden;\r\n\r\n	margin: 0;\r\n	padding: 0;\r\n}\r\n.slick-list:focus\r\n{\r\n	outline: none;\r\n}\r\n.slick-list.dragging\r\n{\r\n	cursor: pointer;\r\n	cursor: hand;\r\n}\r\n\r\n.slick-slider .slick-track,\r\n.slick-slider .slick-list\r\n{\r\n	-webkit-transform: translate3d(0, 0, 0);\r\n	-moz-transform: translate3d(0, 0, 0);\r\n	-ms-transform: translate3d(0, 0, 0);\r\n	-o-transform: translate3d(0, 0, 0);\r\n	transform: translate3d(0, 0, 0);\r\n}\r\n\r\n.slick-track\r\n{\r\n	position: relative;\r\n	top: 0;\r\n	left: 0;\r\n\r\n	display: block;\r\n	margin-left: auto;\r\n	margin-right: auto;\r\n}\r\n.slick-track:before,\r\n.slick-track:after\r\n{\r\n	display: table;\r\n\r\n	content: \'\';\r\n}\r\n.slick-track:after\r\n{\r\n	clear: both;\r\n}\r\n.slick-loading .slick-track\r\n{\r\n	visibility: hidden;\r\n}\r\n\r\n.slick-slide\r\n{\r\n	display: none;\r\n	float: left;\r\n\r\n	height: 100%;\r\n	min-height: 1px;\r\n}\r\n[dir=\'rtl\'] .slick-slide\r\n{\r\n	float: right;\r\n}\r\n.slick-slide img\r\n{\r\n	display: block;\r\n}\r\n.slick-slide.slick-loading img\r\n{\r\n	display: none;\r\n}\r\n.slick-slide.dragging img\r\n{\r\n	pointer-events: none;\r\n}\r\n.slick-initialized .slick-slide\r\n{\r\n	display: block;\r\n}\r\n.slick-loading .slick-slide\r\n{\r\n	visibility: hidden;\r\n}\r\n.slick-vertical .slick-slide\r\n{\r\n	display: block;\r\n\r\n	height: auto;\r\n\r\n	border: 1px solid transparent;\r\n}\r\n.slick-arrow.slick-hidden {\r\n	display: none;\r\n}\r\n\r\n\r\n/* Slider */\r\n.slick-loading .slick-list\r\n{\r\n	background: #fff url(\'./ajax-loader.gif\') center center no-repeat;\r\n}\r\n\r\n/* Icons */\r\n@font-face\r\n{\r\n	font-family: \'slick\';\r\n	font-weight: normal;\r\n	font-style: normal;\r\n\r\n	src: url(\'./fonts/slick.eot\');\r\n	src: url(\'./fonts/slick.eot?#iefix\') format(\'embedded-opentype\'), url(\'./fonts/slick.woff\') format(\'woff\'), url(\'./fonts/slick.ttf\') format(\'truetype\'), url(\'./fonts/slick.svg#slick\') format(\'svg\');\r\n}\r\n/* Arrows */\r\n.slick-prev,\r\n.slick-next\r\n{\r\n	font-size: 0;\r\n	line-height: 0;\r\n\r\n	position: absolute;\r\n	top: 50%;\r\n\r\n	display: block;\r\n\r\n	width: 20px;\r\n	height: 20px;\r\n	padding: 0;\r\n	-webkit-transform: translate(0, -50%);\r\n	-ms-transform: translate(0, -50%);\r\n	transform: translate(0, -50%);\r\n\r\n	cursor: pointer;\r\n\r\n	color: transparent;\r\n	border: none;\r\n	outline: none;\r\n	background: transparent;\r\n}\r\n.slick-prev:hover,\r\n.slick-prev:focus,\r\n.slick-next:hover,\r\n.slick-next:focus\r\n{\r\n	color: transparent;\r\n	outline: none;\r\n	background: transparent;\r\n}\r\n.slick-prev:hover:before,\r\n.slick-prev:focus:before,\r\n.slick-next:hover:before,\r\n.slick-next:focus:before\r\n{\r\n	opacity: 1;\r\n}\r\n.slick-prev.slick-disabled:before,\r\n.slick-next.slick-disabled:before\r\n{\r\n	opacity: .25;\r\n}\r\n\r\n.slick-prev:before,\r\n.slick-next:before\r\n{\r\n	font-family: \'slick\';\r\n	font-size: 20px;\r\n	line-height: 1;\r\n\r\n	opacity: .75;\r\n	color: white;\r\n\r\n	-webkit-font-smoothing: antialiased;\r\n	-moz-osx-font-smoothing: grayscale;\r\n}\r\n\r\n.slick-prev\r\n{\r\n	left: -25px;\r\n}\r\n[dir=\'rtl\'] .slick-prev\r\n{\r\n	right: -25px;\r\n	left: auto;\r\n}\r\n.slick-prev:before\r\n{\r\n	content: \'в†ђ\';\r\n}\r\n[dir=\'rtl\'] .slick-prev:before\r\n{\r\n	content: \'в†’\';\r\n}\r\n\r\n.slick-next\r\n{\r\n	right: -25px;\r\n}\r\n[dir=\'rtl\'] .slick-next\r\n{\r\n	right: auto;\r\n	left: -25px;\r\n}\r\n.slick-next:before\r\n{\r\n	content: \'в†’\';\r\n}\r\n[dir=\'rtl\'] .slick-next:before\r\n{\r\n	content: \'в†ђ\';\r\n}\r\n\r\n/* Dots */\r\n.slick-dotted.slick-slider\r\n{\r\n	margin-bottom: 30px;\r\n}\r\n\r\n.slick-dots\r\n{\r\n	position: absolute;\r\n	bottom: -25px;\r\n\r\n	display: block;\r\n\r\n	width: 100%;\r\n	padding: 0;\r\n	margin: 0;\r\n\r\n	list-style: none;\r\n\r\n	text-align: center;\r\n}\r\n.slick-dots li\r\n{\r\n	position: relative;\r\n\r\n	display: inline-block;\r\n\r\n	width: 20px;\r\n	height: 20px;\r\n	margin: 0 5px;\r\n	padding: 0;\r\n\r\n	cursor: pointer;\r\n}\r\n.slick-dots li button\r\n{\r\n	font-size: 0;\r\n	line-height: 0;\r\n\r\n	display: block;\r\n\r\n	width: 20px;\r\n	height: 20px;\r\n	padding: 5px;\r\n\r\n	cursor: pointer;\r\n\r\n	color: transparent;\r\n	border: 0;\r\n	outline: none;\r\n	background: transparent;\r\n}\r\n.slick-dots li button:hover,\r\n.slick-dots li button:focus\r\n{\r\n	outline: none;\r\n}\r\n.slick-dots li button:hover:before,\r\n.slick-dots li button:focus:before\r\n{\r\n	opacity: 1;\r\n}\r\n.slick-dots li button:before\r\n{\r\n	font-family: \'slick\';\r\n	font-size: 6px;\r\n	line-height: 20px;\r\n\r\n	position: absolute;\r\n	top: 0;\r\n	left: 0;\r\n\r\n	width: 20px;\r\n	height: 20px;\r\n\r\n	content: \'•\';\r\n	text-align: center;\r\n\r\n	opacity: .25;\r\n	color: black;\r\n\r\n	-webkit-font-smoothing: antialiased;\r\n	-moz-osx-font-smoothing: grayscale;\r\n}\r\n.slick-dots li.slick-active button:before\r\n{\r\n	opacity: .75;\r\n	color: black;\r\n}\r\n\r\n\r\n\r\n\r\n\r\nheader{\r\n	position:relative;\r\n	background-image: url(\'assets/templates/img/header-image.png\');\r\n	background-repeat: no-repeat;\r\n	background-size: cover;\r\n	background-position: center;\r\n}\r\n\r\n.navbar-brand img{\r\n	width: 100%;\r\n	margin-top: 20px;\r\n}\r\n\r\n.nav a{\r\n	color: #fff;\r\n}\r\n#production{\r\n	background-image: url(\'assets/templates/img/bg-1.png\');\r\n	background-repeat: no-repeat;\r\n	background-size: cover;\r\n	padding-top: 200px;\r\n	padding-bottom: 160px;\r\n}\r\n\r\n.text{\r\n	position: relative;\r\n	display: inline-block;\r\n}\r\n\r\n.text p{\r\n	font-size: 89px;\r\n	font-family: fantasy;\r\n	color: rgba(99, 22, 0, 0.2);\r\n	position: relative;\r\n	line-height: 1;\r\n}\r\n.clearfix{\r\n	clear: both;\r\n}\r\n\r\n.image img{\r\n	float: left;\r\n}\r\n\r\n.text span{\r\n	position: absolute;\r\n	top: 50px;\r\n	left: 40px;\r\n	color: rgba(97, 25, 0, 0.7);\r\n}\r\n\r\n.prop{\r\n	display: inline-block;\r\n	max-width: 300px;\r\n}\r\n\r\n.product-desc h2{\r\n	color: #540700;\r\n	margin: 30px 0px\r\n}\r\n\r\n.desc p{\r\n	color: #540700;\r\n	font-size: 50px;\r\n	max-width: 75%;\r\n	line-height: 0.9;\r\n	font-family: \'Roboto\', sans-serif;\r\n}\r\n\r\n.firm-name{\r\n	color: rgba(105, 101, 101, 0.7019607843137254);\r\n	font-size: 21px;\r\n}\r\n.product-item{\r\n	width: 40%;\r\n	margin: 0px -86px;\r\n}\r\n.section-name{\r\n	color: rgb(84, 7, 0);\r\n}\r\n.product-link{\r\n	max-width: 270px;\r\n	margin: auto;\r\n	display: block;\r\n	border: 1px solid rgb(84, 7, 0);\r\n	padding: 8px 55px;\r\n	font-size: 18px;\r\n	font-weight: 600;\r\n	color: rgb(84, 7, 0);\r\n	border-radius: 10px;\r\n	min-width: 270px;\r\n	text-align: center;\r\n}\r\n.product-link:hover{\r\n	text-decoration: none;\r\n	color: #983911;\r\n}\r\n.product-img{\r\n	position: relative;\r\n	min-height: 300px;\r\n}\r\n.product-img img{\r\n	width: 100%;\r\n	position: absolute;\r\n	bottom: 0;\r\n}\r\nfooter {\r\n	height: 130px;\r\n	background-image: url(\'assets/templates/img/bg-footer.png\');\r\n	position: relative;\r\n}\r\n\r\n.call-us .form-control{\r\n	border-radius: 1.25rem;\r\n}\r\n\r\n.call-us textarea{\r\n	height: 150px;\r\n}\r\n\r\n.call-us a {\r\n	color: #212529;\r\n}\r\n\r\n.footer-logo{\r\n	width: 100%;\r\n	margin-bottom: 25px;\r\n}\r\n\r\n@media(min-width: 1440px){\r\n	header{\r\n		min-height: 790px;\r\n	}\r\n}\r\n@media(max-width: 1440px){\r\n	#production{\r\n		padding-top: 145px\r\n	}\r\n	header{\r\n		min-height: 720px;\r\n	}\r\n}\r\n@media(max-width: 1024px){\r\n	.product-item{\r\n		width: 50%;\r\n		margin: 0;\r\n		min-height: 350px;\r\n	}\r\n	.image img{\r\n		width: 200px;\r\n	}\r\n	.desc p{\r\n		font-size: 33px;\r\n	}\r\n	.desc{\r\n		width: 550px;\r\n	}\r\n}\r\n\r\nimg.text{\r\n	height: 405px;\r\n}\r\n\r\n@media(max-width: 768px){\r\n	#production {\r\n		padding-top: 40px;\r\n	}\r\n	.header-slick {\r\n		display: none;\r\n	}\r\n}\r\n@media(max-width: 425px){\r\n	.product-item{\r\n		min-height: auto;\r\n	}\r\n	.product-img{\r\n		min-height: 170px;\r\n	}\r\n	.desc p{\r\n		font-size: 20px;\r\n	}\r\n	img.text {\r\n		height: 285px;\r\n	}\r\n\r\n}\r\n@media(max-width: 375px){\r\n	.product-item{\r\n		width: 100%;\r\n		margin-top: 90px;\r\n	}\r\n	.image img{\r\n		width: 200px;\r\n	}\r\n	.desc p{\r\n		font-size: 20px;\r\n	}\r\n}\r\n\r\nform .product-link{\r\n	background: #fff;\r\n	margin-bottom: 10px;\r\n}\r\n\r\n.product-desc{\r\n	margin: 70px 0px;\r\n}\r\n\r\n.up{\r\n	position: fixed;\r\n	bottom: 45px;\r\n	right: 20px;\r\n	width: 50px;\r\n}\r\n.up img{\r\n	width: 100%;\r\n}\r\n\r\n#call-us{\r\n	margin-top:125px;\r\n	margin-bottom: 50px;\r\n}\r\n\r\n#products .section-name{\r\n	margin-bottom: 40px;\r\n}\r\n\r\n.prod{\r\n	display: flex;\r\n	justify-content: center;\r\n	align-items: center;\r\n	padding: 0 15px;\r\n}\r\n\r\n.product-image img{\r\n	max-width: 190px;\r\n}\r\n\r\n.product-text p{\r\n	/*line-height: 25px;*/\r\n	text-align: justify;\r\n}\r\n\r\n.product-text h2{\r\n	color: rgb(84, 7, 0);\r\n}\r\n\r\n\r\n@media(max-width:572px){\r\n	.prod{\r\n		flex-direction: column;\r\n	}\r\n}\r\n\r\n.podpis{\r\n	position: absolute;\r\n    bottom: 10px;\r\n    width: 100px;\r\n    left: 0;\r\n    right: 0;\r\n    margin: auto;\r\n}\r\n\r\n.podpis img{\r\n	width: 150px;\r\n}\r\n\r\n/*.product-desc p{\r\n	line-height: 20px;\r\n}*/\r\n\r\n#products .product-text a {\r\n    font-size: 2rem;\r\n    color: rgb(84, 7, 0);\r\n    text-decoration: none;\r\n}\r\n\r\n.pi .img-tov1 img{\r\n	width: 100%;\r\n}\r\n\r\n.pi p{\r\n	text-align: center;\r\n}\r\n\r\n.pi .pagetitle{\r\n    font-size: 23px;\r\n    font-weight: 700;\r\n    color: #540700;\r\n	min-height: 50px;\r\n}\r\n\r\n.img-tov{\r\n	\r\n}\r\n\r\n.header-nav-links {\r\n	margin-bottom: auto;\r\n	padding-top: 56px;\r\n	text-transform: uppercase;\r\n}\r\n\r\n.tov-items {\r\n	overflow: hidden;\r\n}\r\n\r\n\r\n#production {\r\n	overflow: hidden;\r\n}\r\n\r\n@media screen and (min-width: 961px){\r\n	.navbar {\r\n		flex-wrap: nowrap;\r\n	}\r\n\r\n	.header-nav-links {\r\n	    margin-bottom: auto;\r\n	    padding-top: 56px;\r\n	}\r\n}\r\n\r\n@media screen and (min-width: 1200px){\r\n	.navbar {\r\n		flex-wrap: nowrap;\r\n	}\r\n\r\n	.header-slick {\r\n	    margin-top: -9rem;\r\n	    margin-left: -4.5rem;\r\n	}\r\n\r\n	#products {\r\n		position: relative;\r\n		margin-top: -8rem;\r\n	}\r\n\r\n	.product-image {\r\n		margin-right: 4rem;\r\n	}\r\n}\r\n\r\n@media screen and (max-width: 1200px){\r\n	.product-link {\r\n		max-width: 200px;\r\n	}\r\n}\r\n\r\n		\r\n.header-slick {\r\n	max-width: 280px;\r\n}\r\n\r\n.img-tov1{\r\n	width: 350px;\r\n	height: 350px;\r\n	overflow: hidden;\r\n}\r\n\r\n.img-tov1 img {\r\n -moz-transition: all 1s ease-out;\r\n -o-transition: all 1s ease-out;\r\n -webkit-transition: all 1s ease-out;\r\n }\r\n\r\n.img-tov1 img:hover{\r\n -webkit-transform: scale(1.1);\r\n -moz-transform: scale(1.1);\r\n -o-transform: scale(1.1);\r\n }\r\n\r\n', 0, 0, 6, 0, 0, 1, 1524771272, 2, 1524821221, 0, 0, 0, 1524771272, 1, '', 1, 0, 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_htmlsnippets`
--

CREATE TABLE `aw36_site_htmlsnippets` (
  `id` int(10) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Chunk',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `editor_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `snippet` mediumtext COLLATE utf8_unicode_ci,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Disables the snippet'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains the site chunks.';

--
-- Дамп данных таблицы `aw36_site_htmlsnippets`
--

INSERT INTO `aw36_site_htmlsnippets` (`id`, `name`, `description`, `editor_type`, `editor_name`, `category`, `cache_type`, `snippet`, `locked`, `createdon`, `editedon`, `disabled`) VALUES
(1, 'mm_rules', 'Default ManagerManager rules.', 0, 'none', 2, 0, '// more example rules are in assets/plugins/managermanager/example_mm_rules.inc.php\n// example of how PHP is allowed - check that a TV named documentTags exists before creating rule\n\nif ($modx->db->getValue($modx->db->select(\'count(id)\', $modx->getFullTableName(\'site_tmplvars\'), \"name=\'documentTags\'\"))) {\n	mm_widget_tags(\'documentTags\', \' \'); // Give blog tag editing capabilities to the \'documentTags (3)\' TV\n}\nmm_widget_showimagetvs(); // Always give a preview of Image TVs\n\nmm_createTab(\'SEO\', \'seo\', \'\', \'\', \'\', \'\');\nmm_moveFieldsToTab(\'titl,keyw,desc,seoOverride,noIndex,sitemap_changefreq,sitemap_priority,sitemap_exclude\', \'seo\', \'\', \'\');\nmm_widget_tags(\'keyw\',\',\'); // Give blog tag editing capabilities to the \'documentTags (3)\' TV\n\n\n//mm_createTab(\'Images\', \'photos\', \'\', \'\', \'\', \'850\');\n//mm_moveFieldsToTab(\'images,photos\', \'photos\', \'\', \'\');\n\n//mm_hideFields(\'longtitle,description,link_attributes,menutitle,content\', \'\', \'6,7\');\n\n//mm_hideTemplates(\'0,5,8,9,11,12\', \'2,3\');\n\n//mm_hideTabs(\'settings, access\', \'2\');\n', 0, 0, 0, 0),
(2, 'head', 'sample header scripts', 2, 'none', 3, 0, '<head>\r\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=[(modx_charset)]\" /> \r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\r\n    <link rel=\"stylesheet\" href=\"/assets\\templates\\css\\bootstrap.css\">\r\n    <link rel=\"stylesheet\" href=\"/assets\\templates\\css\\animate.css\">\r\n    <link rel=\"stylesheet\" href=\"/[~19~]\">\r\n    <link href=\"https://fonts.googleapis.com/css?family=Roboto\" rel=\"stylesheet\">\r\n	<link href=\"https://fonts.googleapis.com/css?family=Montserrat\" rel=\"stylesheet\">\r\n    <script src=\"/assets\\templates\\js\\jquery-3.3.1.min.js\"></script>\r\n	<script src=\"/[~2~]\"></script>\r\n    <script src=\"/assets\\templates\\js\\slick.js\"></script>\r\n    <script src=\"/assets\\templates\\js\\bootstrap.js\"></script>\r\n    <title>[*titl*]</title>\r\n	[*noIndex*]\r\n	<meta name=\"keywords\" content=\"[*keyw*]\" />\r\n	<meta name=\"description\" content=\"[*desc*]\" />\r\n	<meta property=\"og:title\" content=\"Article on Anything by You\" />\r\n	<meta property=\"og:type\" content=\"article\" />\r\n	<meta property=\"og:url\" content=\"[(site_url)]\" />\r\n	<meta itemprop=\"description\" content=\"\" />\r\n	<meta property=\"og:image\" content=\"/assets/templates/img/header-image.png\" />\r\n	<base href=\"[(site_url)]\"/>\r\n</head>', 0, 0, 1524820193, 0),
(3, 'feedback', '<strong>1.0</strong> форма обратной связи', 0, 'none', 7, 0, '[!eForm? &formid=`feedbackForm` &subject=`Сообщение с сайта` &tpl=`eFeedbackForm` &report=`eFeedbackReport` &gotoid=`[*id*]` &vericode=`1` !] \n\n', 0, 0, 0, 0),
(4, 'eFeedbackReport', '<strong>1.0</strong> eFeedbackReport  шаблон отправки на почту', 0, 'none', 7, 0, '<p>Прислано человеком, с именем: [+name+] . Подробности ниже:</p>\n<table>\n<tr valign=\"top\"><td>Имя:</td><td>[+name+]</td></tr>\n<tr valign=\"top\"><td>E-mail:</td><td>[+email+]</td></tr>\n<tr valign=\"top\"><td>Номер телефона:</td><td>[+phone+]</td></tr>\n<tr valign=\"top\"><td>Текст сообщения:</td><td>[+comments+]</td></tr>\n</table>\n<p>Можно использовать ссылку для ответа: <a href=\"mailto:[+email+]?subject=RE:[+subject+]\">[+email+]</a></p>\n\n', 0, 0, 0, 0),
(5, 'eFeedbackForm', '<strong>1.0</strong> eFeedbackForm Шаблон формы обратной связи', 0, 'none', 7, 0, '<p><span style=\"color:#900;\">[+validationmessage+]</span></p>\n\n<form  class=\"eform\" method=\"post\" action=\"[~[*id*]~]\">\n\n<input type=\"hidden\" name=\"formid\" value=\"feedbackForm\" />\n<input value=\"\" name=\"special\" class=\"special\" type=\"text\" eform=\"Спец:date:0\"  style=\"display:none;\" />\n<p>\n    <input type=\"text\" name=\"name\" id=\"name\" class=\"grid_3\" value=\"\"  eform=\"Имя:string:1\"/>\n    <label for=\"name\">Ваше имя</label>\n</p>\n            \n<p>\n    <input type=\"text\" name=\"email\" id=\"email\" class=\"grid_3\" value=\"\" eform=\"E-mail:email:1\" />\n    <label for=\"email\">Ваш E-mail</label>\n</p>\n            \n<p>\n    <input type=\"text\" name=\"phone\" id=\"subject\" class=\"grid_3\" value=\"\" eform=\"Номер телефона:string:1\"/>\n    <label for=\"subject\">Номер телефона</label>\n</p>\n            \n<p>\n    <textarea name=\"comments\" id=\"message\" class=\"grid_6\" cols=\"50\" rows=\"10\" eform=\"Текст сообщения:string:1\"></textarea>\n</p>\n<p>Введите код с картинки: <br />\n    <input type=\"text\" class=\"ver\" name=\"vericode\" /><img class=\"feed\" src=\"[+verimageurl+]\" alt=\"Введите код\" />\n</p>            \n<p>\n    <input type=\"submit\" name=\"submit\" class=\"subeform grid_2\" value=\"Отправить сообщение\"/>\n </p>\n\n</form>\n\n\n \n\n', 0, 0, 0, 0),
(6, 'footer', '', 2, 'none', 0, 0, '<footer>\n			<div class=\"podpis\"><img src=\"assets/images/podpis-ruslanyushkevich.svg\" alt=\"\"></div>\n		</footer>\n		<div class=\"up\"><img src=\"assets/images/w128h1281338911579arrowup21.png\" alt=\"\"></div>', 0, 1524305379, 1524305379, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_modules`
--

CREATE TABLE `aw36_site_modules` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `wrap` tinyint(4) NOT NULL DEFAULT '0',
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'url to module icon',
  `enable_resource` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'enables the resource file feature',
  `resourcefile` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'a physical link to a resource file',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  `guid` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'globally unique identifier',
  `enable_sharedparams` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text COLLATE utf8_unicode_ci,
  `modulecode` mediumtext COLLATE utf8_unicode_ci COMMENT 'module boot up code'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Site Modules';

--
-- Дамп данных таблицы `aw36_site_modules`
--

INSERT INTO `aw36_site_modules` (`id`, `name`, `description`, `editor_type`, `disabled`, `category`, `wrap`, `locked`, `icon`, `enable_resource`, `resourcefile`, `createdon`, `editedon`, `guid`, `enable_sharedparams`, `properties`, `modulecode`) VALUES
(1, 'Doc Manager', '<strong>1.1</strong> Quickly perform bulk updates to the Documents in your site including templates, publishing details, and permissions', 0, 0, 4, 0, 0, '', 0, '', 0, 0, 'docman435243542tf542t5t', 1, '', ' \n/**\n * Doc Manager\n * \n * Quickly perform bulk updates to the Documents in your site including templates, publishing details, and permissions\n * \n * @category	module\n * @version 	1.1\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties\n * @internal	@guid docman435243542tf542t5t	\n * @internal	@shareparams 1\n * @internal	@dependencies requires files located at /assets/modules/docmanager/\n * @internal	@modx_category Manager and Admin\n * @internal    @installset base, sample\n * @lastupdate  09/04/2016\n */\n\ninclude_once(MODX_BASE_PATH.\'assets/modules/docmanager/classes/docmanager.class.php\');\ninclude_once(MODX_BASE_PATH.\'assets/modules/docmanager/classes/dm_frontend.class.php\');\ninclude_once(MODX_BASE_PATH.\'assets/modules/docmanager/classes/dm_backend.class.php\');\n\n$dm = new DocManager($modx);\n$dmf = new DocManagerFrontend($dm, $modx);\n$dmb = new DocManagerBackend($dm, $modx);\n\n$dm->ph = $dm->getLang();\n$dm->ph[\'theme\'] = $dm->getTheme();\n$dm->ph[\'ajax.endpoint\'] = MODX_SITE_URL.\'assets/modules/docmanager/tv.ajax.php\';\n$dm->ph[\'datepicker.offset\'] = $modx->config[\'datepicker_offset\'];\n$dm->ph[\'datetime.format\'] = $modx->config[\'datetime_format\'];\n\nif (isset($_POST[\'tabAction\'])) {\n    $dmb->handlePostback();\n} else {\n    $dmf->getViews();\n    echo $dm->parseTemplate(\'main.tpl\', $dm->ph);\n}'),
(2, 'Extras', '<strong>0.1.3</strong> first repository for Evolution CMS', 0, 0, 4, 0, 0, '', 0, '', 0, 0, 'store435243542tf542t5t', 1, '', ' \n/**\n * Extras\n * \n * first repository for Evolution CMS\n * \n * @category	module\n * @version 	0.1.3\n * @internal	@properties\n * @internal	@guid store435243542tf542t5t	\n * @internal	@shareparams 1\n * @internal	@dependencies requires files located at /assets/modules/store/\n * @internal	@modx_category Manager and Admin\n * @internal    @installset base, sample\n * @lastupdate  25/11/2016\n */\n\n//AUTHORS: Bumkaka & Dmi3yy \ninclude_once(\'../assets/modules/store/core.php\');');

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_module_access`
--

CREATE TABLE `aw36_site_module_access` (
  `id` int(10) UNSIGNED NOT NULL,
  `module` int(11) NOT NULL DEFAULT '0',
  `usergroup` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Module users group access permission';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_module_depobj`
--

CREATE TABLE `aw36_site_module_depobj` (
  `id` int(11) NOT NULL,
  `module` int(11) NOT NULL DEFAULT '0',
  `resource` int(11) NOT NULL DEFAULT '0',
  `type` int(2) NOT NULL DEFAULT '0' COMMENT '10-chunks, 20-docs, 30-plugins, 40-snips, 50-tpls, 60-tvs'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Module Dependencies';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_plugins`
--

CREATE TABLE `aw36_site_plugins` (
  `id` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Plugin',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `plugincode` mediumtext COLLATE utf8_unicode_ci,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text COLLATE utf8_unicode_ci COMMENT 'Default Properties',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Disables the plugin',
  `moduleguid` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID of module from which to import shared parameters',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains the site plugins.';

--
-- Дамп данных таблицы `aw36_site_plugins`
--

INSERT INTO `aw36_site_plugins` (`id`, `name`, `description`, `editor_type`, `category`, `cache_type`, `plugincode`, `locked`, `properties`, `disabled`, `moduleguid`, `createdon`, `editedon`) VALUES
(1, 'Quick Manager+', '<strong>1.5.10</strong> Enables QuickManager+ front end content editing support', 0, 4, 0, '\n/**\n * Quick Manager+\n * \n * Enables QuickManager+ front end content editing support\n *\n * @category 	plugin\n * @version 	1.5.10\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL v3)\n * @internal    @properties &jqpath=Path to jQuery;text;assets/js/jquery.min.js &loadmanagerjq=Load jQuery in manager;list;true,false;false &loadfrontendjq=Load jQuery in front-end;list;true,false;false &noconflictjq=jQuery noConflict mode in front-end;list;true,false;false &loadfa=Load Font Awesome css in front-end;list;true,false;true &loadtb=Load modal box in front-end;list;true,false;true &tbwidth=Modal box window width;text;80% &tbheight=Modal box window height;text;90% &hidefields=Hide document fields from front-end editors;text;parent &hidetabs=Hide document tabs from front-end editors;text; &hidesections=Hide document sections from front-end editors;text; &addbutton=Show add document here button;list;true,false;true &tpltype=New document template type;list;parent,id,selected;parent &tplid=New document template id;int;3 &custombutton=Custom buttons;textarea; &managerbutton=Show go to manager button;list;true,false;true &logout=Logout to;list;manager,front-end;manager &disabled=Plugin disabled on documents;text; &autohide=Autohide toolbar;list;true,false;true &position= Toolbar position;list;top,right,bottom,left,before;top &editbuttons=Inline edit buttons;list;true,false;false &editbclass=Edit button CSS class;text;qm-edit &newbuttons=Inline new resource buttons;list;true,false;false &newbclass=New resource button CSS class;text;qm-new &tvbuttons=Inline template variable buttons;list;true,false;false &tvbclass=Template variable button CSS class;text;qm-tv &removeBg=Remove toolbar background;list;yes,no;no &buttonStyle=QuickManager buttons CSS stylesheet;list;actionButtons,navButtons;navButtons  \n * @internal	@events OnParseDocument,OnWebPagePrerender,OnDocFormPrerender,OnDocFormSave,OnManagerLogout \n * @internal	@modx_category Manager and Admin\n * @internal    @legacy_names QM+,QuickEdit\n * @internal    @installset base, sample\n * @internal    @disabled 1\n * @reportissues https://github.com/modxcms/evolution\n * @documentation Official docs [+site_url+]assets/plugins/qm/readme.html\n * @link        http://www.maagit.fi/modx/quickmanager-plus\n * @author      Mikko Lammi\n * @author      Since 2011: yama, dmi3yy, segr, Nicola1971.\n * @lastupdate  02/02/2018 \n */\n\n// In manager\nif (!$modx->checkSession()) return;\n\n$show = TRUE;\n\nif ($disabled  != \'\') {\n    $arr = array_filter(array_map(\'intval\', explode(\',\', $disabled)));\n    if (in_array($modx->documentIdentifier, $arr)) {\n        $show = FALSE;\n    }\n}\n\nif ($show) {\n    // Replace [*#tv*] with QM+ edit TV button placeholders\n    if ($tvbuttons == \'true\') {\n        if ($modx->event->name == \'OnParseDocument\') {\n             $output = &$modx->documentOutput;\n             $output = preg_replace(\'~\\[\\*#(.*?)\\*\\]~\', \'<!-- \'.$tvbclass.\' $1 -->[*$1*]\', $output);\n             $modx->documentOutput = $output;\n         }\n     }\n    include_once($modx->config[\'base_path\'].\'assets/plugins/qm/qm.inc.php\');\n    $qm = new Qm($modx, $jqpath, $loadmanagerjq, $loadfrontendjq, $noconflictjq, $loadfa, $loadtb, $tbwidth, $tbheight, $hidefields, $hidetabs, $hidesections, $addbutton, $tpltype, $tplid, $custombutton, $managerbutton, $logout, $autohide, $position, $editbuttons, $editbclass, $newbuttons, $newbclass, $tvbuttons, $tvbclass, $buttonStyle, $removeBg);\n}\n', 0, '{\"jqpath\":[{\"label\":\"Path to jQuery\",\"type\":\"text\",\"value\":\"assets\\/js\\/jquery.min.js\",\"default\":\"assets\\/js\\/jquery.min.js\",\"desc\":\"\"}],\"loadmanagerjq\":[{\"label\":\"Load jQuery in manager\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"loadfrontendjq\":[{\"label\":\"Load jQuery in front-end\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"noconflictjq\":[{\"label\":\"jQuery noConflict mode in front-end\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"loadfa\":[{\"label\":\"Load Font Awesome css in front-end\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"loadtb\":[{\"label\":\"Load modal box in front-end\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"tbwidth\":[{\"label\":\"Modal box window width\",\"type\":\"text\",\"value\":\"80%\",\"default\":\"80%\",\"desc\":\"\"}],\"tbheight\":[{\"label\":\"Modal box window height\",\"type\":\"text\",\"value\":\"90%\",\"default\":\"90%\",\"desc\":\"\"}],\"hidefields\":[{\"label\":\"Hide document fields from front-end editors\",\"type\":\"text\",\"value\":\"parent\",\"default\":\"parent\",\"desc\":\"\"}],\"hidetabs\":[{\"label\":\"Hide document tabs from front-end editors\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"hidesections\":[{\"label\":\"Hide document sections from front-end editors\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"addbutton\":[{\"label\":\"Show add document here button\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"tpltype\":[{\"label\":\"New document template type\",\"type\":\"list\",\"value\":\"parent\",\"options\":\"parent,id,selected\",\"default\":\"parent\",\"desc\":\"\"}],\"tplid\":[{\"label\":\"New document template id\",\"type\":\"int\",\"value\":\"3\",\"default\":\"3\",\"desc\":\"\"}],\"custombutton\":[{\"label\":\"Custom buttons\",\"type\":\"textarea\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"managerbutton\":[{\"label\":\"Show go to manager button\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"logout\":[{\"label\":\"Logout to\",\"type\":\"list\",\"value\":\"manager\",\"options\":\"manager,front-end\",\"default\":\"manager\",\"desc\":\"\"}],\"disabled\":[{\"label\":\"Plugin disabled on documents\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"autohide\":[{\"label\":\"Autohide toolbar\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"position\":[{\"label\":\"Toolbar position\",\"type\":\"list\",\"value\":\"top\",\"options\":\"top,right,bottom,left,before\",\"default\":\"top\",\"desc\":\"\"}],\"editbuttons\":[{\"label\":\"Inline edit buttons\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"editbclass\":[{\"label\":\"Edit button CSS class\",\"type\":\"text\",\"value\":\"qm-edit\",\"default\":\"qm-edit\",\"desc\":\"\"}],\"newbuttons\":[{\"label\":\"Inline new resource buttons\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"newbclass\":[{\"label\":\"New resource button CSS class\",\"type\":\"text\",\"value\":\"qm-new\",\"default\":\"qm-new\",\"desc\":\"\"}],\"tvbuttons\":[{\"label\":\"Inline template variable buttons\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"tvbclass\":[{\"label\":\"Template variable button CSS class\",\"type\":\"text\",\"value\":\"qm-tv\",\"default\":\"qm-tv\",\"desc\":\"\"}],\"removeBg\":[{\"label\":\"Remove toolbar background\",\"type\":\"list\",\"value\":\"no\",\"options\":\"yes,no\",\"default\":\"no\",\"desc\":\"\"}],\"buttonStyle\":[{\"label\":\"QuickManager buttons CSS stylesheet\",\"type\":\"list\",\"value\":\"navButtons\",\"options\":\"actionButtons,navButtons\",\"default\":\"navButtons\",\"desc\":\"\"}]}', 1, '', 0, 0),
(2, 'Updater', '<strong>0.8.4</strong> show message about outdated CMS version', 0, 4, 0, 'require MODX_BASE_PATH.\'assets/plugins/updater/plugin.updater.php\';\n\n\n', 0, '{\"version\":[{\"label\":\"Version:\",\"type\":\"text\",\"value\":\"evolution-cms\\/evolution\",\"default\":\"evolution-cms\\/evolution\",\"desc\":\"\"}],\"wdgVisibility\":[{\"label\":\"Show widget for:\",\"type\":\"menu\",\"value\":\"All\",\"options\":\"All,AdminOnly,AdminExcluded,ThisRoleOnly,ThisUserOnly\",\"default\":\"All\",\"desc\":\"\"}],\"ThisRole\":[{\"label\":\"Show only to this role id:\",\"type\":\"string\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"ThisUser\":[{\"label\":\"Show only to this username:\",\"type\":\"string\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"showButton\":[{\"label\":\"Show Update Button:\",\"type\":\"menu\",\"value\":\"AdminOnly\",\"options\":\"show,hide,AdminOnly\",\"default\":\"AdminOnly\",\"desc\":\"\"}],\"type\":[{\"label\":\"Type:\",\"type\":\"menu\",\"value\":\"tags\",\"options\":\"tags,releases,commits\",\"default\":\"tags\",\"desc\":\"\"}],\"branch\":[{\"label\":\"Commit branch:\",\"type\":\"text\",\"value\":\"develop\",\"default\":\"develop\",\"desc\":\"\"}]}', 0, '', 0, 0),
(3, 'FileSource', '<strong>0.1</strong> Save snippet and plugins to file', 0, 4, 0, 'require MODX_BASE_PATH.\'assets/plugins/filesource/plugin.filesource.php\';', 0, '', 0, '', 0, 0),
(4, 'Forgot Manager Login', '<strong>1.1.7</strong> Resets your manager login when you forget your password via email confirmation', 0, 4, 0, 'require MODX_BASE_PATH.\'assets/plugins/forgotmanagerlogin/plugin.forgotmanagerlogin.php\';', 0, '', 0, '', 0, 0),
(5, 'userHelper', '<strong>1.7.18</strong> addition to FormLister', 0, 5, 0, '\n/**\n * userHelper\n * \n * addition to FormLister\n * \n * @category    plugin\n * @version     1.7.18\n * @internal    @properties &logoutKey=Request key;text;logout &cookieName=Cookie Name;text;WebLoginPE &cookieLifetime=Cookie Lifetime, seconds;text;157680000 &maxFails=Max failed logins;text;3 &blockTime=Block for, seconds;text;3600\n * @internal    @events OnWebAuthentication,OnWebPageInit,OnPageNotFound,OnWebLogin\n * @internal    @modx_category Content\n * @internal    @disabled 1\n**/\n\nrequire MODX_BASE_PATH.\'assets/snippets/FormLister/plugin.userHelper.php\';\n', 0, '{\"logoutKey\":[{\"label\":\"Request key\",\"type\":\"text\",\"value\":\"logout\",\"default\":\"logout\",\"desc\":\"\"}],\"cookieName\":[{\"label\":\"Cookie Name\",\"type\":\"text\",\"value\":\"WebLoginPE\",\"default\":\"WebLoginPE\",\"desc\":\"\"}],\"cookieLifetime\":[{\"label\":\"Cookie Lifetime, seconds\",\"type\":\"text\",\"value\":\"157680000\",\"default\":\"157680000\",\"desc\":\"\"}],\"maxFails\":[{\"label\":\"Max failed logins\",\"type\":\"text\",\"value\":\"3\",\"default\":\"3\",\"desc\":\"\"}],\"blockTime\":[{\"label\":\"Block for, seconds\",\"type\":\"text\",\"value\":\"3600\",\"default\":\"3600\",\"desc\":\"\"}]}', 1, '', 0, 0),
(6, 'ManagerManager', '<strong>0.6.3</strong> Customize the EVO Manager to offer bespoke admin functions for end users or manipulate the display of document fields in the manager.', 0, 4, 0, '\n/**\n * ManagerManager\n *\n * Customize the EVO Manager to offer bespoke admin functions for end users or manipulate the display of document fields in the manager.\n *\n * @category plugin\n * @version 0.6.3\n * @license http://creativecommons.org/licenses/GPL/2.0/ GNU Public License (GPL v2)\n * @internal @properties &remove_deprecated_tv_types_pref=Remove deprecated TV types;list;yes,no;yes &config_chunk=Configuration Chunk;text;mm_rules\n * @internal @events OnDocFormRender,OnDocFormPrerender,OnBeforeDocFormSave,OnDocFormSave,OnDocDuplicate,OnPluginFormRender,OnTVFormRender\n * @internal @modx_category Manager and Admin\n * @internal @installset base\n * @internal @legacy_names Image TV Preview, Show Image TVs\n * @reportissues https://github.com/DivanDesign/MODXEvo.plugin.ManagerManager/\n * @documentation README [+site_url+]assets/plugins/managermanager/readme.html\n * @documentation Official docs http://code.divandesign.biz/modx/managermanager\n * @link        Latest version http://code.divandesign.biz/modx/managermanager\n * @link        Additional tools http://code.divandesign.biz/modx\n * @link        Full changelog http://code.divandesign.biz/modx/managermanager/changelog\n * @author      Inspired by: HideEditor plugin by Timon Reinhard and Gildas; HideManagerFields by Brett @ The Man Can!\n * @author      DivanDesign studio http://www.DivanDesign.biz\n * @author      Nick Crossland http://www.rckt.co.uk\n * @author      Many others\n * @lastupdate  22/01/2018\n */\n\n// Run the main code\ninclude($modx->config[\'base_path\'].\'assets/plugins/managermanager/mm.inc.php\');\n', 0, '{\"remove_deprecated_tv_types_pref\":[{\"label\":\"Remove deprecated TV types\",\"type\":\"list\",\"value\":\"yes\",\"options\":\"yes,no\",\"default\":\"yes\",\"desc\":\"\"}],\"config_chunk\":[{\"label\":\"Configuration Chunk\",\"type\":\"text\",\"value\":\"mm_rules\",\"default\":\"mm_rules\",\"desc\":\"\"}]}', 0, '', 0, 0),
(7, 'CodeMirror', '<strong>1.5</strong> JavaScript library that can be used to create a relatively pleasant editor interface based on CodeMirror 5.33 (released on 21-12-2017)', 0, 4, 0, '\n/**\n * CodeMirror\n *\n * JavaScript library that can be used to create a relatively pleasant editor interface based on CodeMirror 5.33 (released on 21-12-2017)\n *\n * @category    plugin\n * @version     1.5\n * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @package     evo\n * @internal    @events OnDocFormRender,OnChunkFormRender,OnModFormRender,OnPluginFormRender,OnSnipFormRender,OnTempFormRender,OnRichTextEditorInit\n * @internal    @modx_category Manager and Admin\n * @internal    @properties &theme=Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,one-dark,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light;default &darktheme=Dark Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,one-dark,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light;one-dark &fontSize=Font-size;list;10,11,12,13,14,15,16,17,18;14 &lineHeight=Line-height;list;1,1.1,1.2,1.3,1.4,1.5;1.3 &indentUnit=Indent unit;int;4 &tabSize=The width of a tab character;int;4 &lineWrapping=lineWrapping;list;true,false;true &matchBrackets=matchBrackets;list;true,false;true &activeLine=activeLine;list;true,false;false &emmet=emmet;list;true,false;true &search=search;list;true,false;false &indentWithTabs=indentWithTabs;list;true,false;true &undoDepth=undoDepth;int;200 &historyEventDelay=historyEventDelay;int;1250\n * @internal    @installset base\n * @reportissues https://github.com/evolution-cms/evolution/issues/\n * @documentation Official docs https://codemirror.net/doc/manual.html\n * @author      hansek from http://www.modxcms.cz\n * @author      update Mihanik71\n * @author      update Deesen\n * @author      update 64j\n * @lastupdate  08-01-2018\n */\n\n$_CM_BASE = \'assets/plugins/codemirror/\';\n\n$_CM_URL = $modx->config[\'site_url\'] . $_CM_BASE;\n\nrequire(MODX_BASE_PATH. $_CM_BASE .\'codemirror.plugin.php\');', 0, '{\"theme\":[{\"label\":\"Theme\",\"type\":\"list\",\"value\":\"default\",\"options\":\"default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,one-dark,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light\",\"default\":\"default\",\"desc\":\"\"}],\"darktheme\":[{\"label\":\"Dark Theme\",\"type\":\"list\",\"value\":\"one-dark\",\"options\":\"default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,one-dark,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light\",\"default\":\"one-dark\",\"desc\":\"\"}],\"fontSize\":[{\"label\":\"Font-size\",\"type\":\"list\",\"value\":\"14\",\"options\":\"10,11,12,13,14,15,16,17,18\",\"default\":\"14\",\"desc\":\"\"}],\"lineHeight\":[{\"label\":\"Line-height\",\"type\":\"list\",\"value\":\"1.3\",\"options\":\"1,1.1,1.2,1.3,1.4,1.5\",\"default\":\"1.3\",\"desc\":\"\"}],\"indentUnit\":[{\"label\":\"Indent unit\",\"type\":\"int\",\"value\":\"4\",\"default\":\"4\",\"desc\":\"\"}],\"tabSize\":[{\"label\":\"The width of a tab character\",\"type\":\"int\",\"value\":\"4\",\"default\":\"4\",\"desc\":\"\"}],\"lineWrapping\":[{\"label\":\"lineWrapping\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"matchBrackets\":[{\"label\":\"matchBrackets\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"activeLine\":[{\"label\":\"activeLine\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"emmet\":[{\"label\":\"emmet\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"search\":[{\"label\":\"search\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"indentWithTabs\":[{\"label\":\"indentWithTabs\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"undoDepth\":[{\"label\":\"undoDepth\",\"type\":\"int\",\"value\":\"200\",\"default\":\"200\",\"desc\":\"\"}],\"historyEventDelay\":[{\"label\":\"historyEventDelay\",\"type\":\"int\",\"value\":\"1250\",\"default\":\"1250\",\"desc\":\"\"}]}', 0, '', 0, 0),
(8, 'ElementsInTree', '<strong>1.5.9</strong> Get access to all Elements and Modules inside Manager sidebar', 0, 4, 0, 'require MODX_BASE_PATH.\'assets/plugins/elementsintree/plugin.elementsintree.php\';\n', 0, '{\"adminRoleOnly\":[{\"label\":\"Administrators only\",\"type\":\"list\",\"value\":\"yes\",\"options\":\"yes,no\",\"default\":\"yes\",\"desc\":\"\"}],\"treeButtonsInTab\":[{\"label\":\"Tree buttons in tab\",\"type\":\"list\",\"value\":\"yes\",\"options\":\"yes,no\",\"default\":\"yes\",\"desc\":\"\"}]}', 1, '', 0, 0),
(9, 'TinyMCE4', '<strong>4.7.4</strong> Javascript rich text editor', 0, 4, 0, '\n/**\n * TinyMCE4\n *\n * Javascript rich text editor\n *\n * @category    plugin\n * @version     4.7.4\n * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal    @properties &styleFormats=Custom Style Formats <b>RAW</b><br/><br/><ul><li>leave empty to use below block/inline formats</li><li>allows simple-format: <i>Title,cssClass|Title2,cssClass2</i></li><li>Also accepts full JSON-config as per TinyMCE4 docs / configure / content-formating / style_formats</li></ul>;textarea; &styleFormats_inline=Custom Style Formats <b>INLINE</b><br/><br/><ul><li>will wrap selected text with span-tag + css-class</li><li>simple-format only</li></ul>;textarea;InlineTitle,cssClass1|InlineTitle2,cssClass2 &styleFormats_block=Custom Style Formats <b>BLOCK</b><br/><br/><ul><li>will add css-class to selected block-element</li><li>simple-format only</li></ul>;textarea;BlockTitle,cssClass3|BlockTitle2,cssClass4 &customParams=Custom Parameters<br/><b>(Be careful or leave empty!)</b>;textarea; &entityEncoding=Entity Encoding;list;named,numeric,raw;named &entities=Entities;text; &pathOptions=Path Options;list;Site config,Absolute path,Root relative,URL,No convert;Site config &resizing=Advanced Resizing;list;true,false;false &disabledButtons=Disabled Buttons;text; &webTheme=Web Theme;test;webuser &webPlugins=Web Plugins;text; &webButtons1=Web Buttons 1;text;bold italic underline strikethrough removeformat alignleft aligncenter alignright &webButtons2=Web Buttons 2;text;link unlink image undo redo &webButtons3=Web Buttons 3;text; &webButtons4=Web Buttons 4;text; &webAlign=Web Toolbar Alignment;list;ltr,rtl;ltr &width=Width;text;100% &height=Height;text;400px &introtextRte=<b>Introtext RTE</b><br/>add richtext-features to \"introtext\";list;enabled,disabled;disabled &inlineMode=<b>Inline-Mode</b>;list;enabled,disabled;disabled &inlineTheme=<b>Inline-Mode</b><br/>Theme;text;inline &browser_spellcheck=<b>Browser Spellcheck</b><br/>At least one dictionary must be installed inside your browser;list;enabled,disabled;disabled &paste_as_text=<b>Force Paste as Text</b>;list;enabled,disabled;disabled\n * @internal    @events OnLoadWebDocument,OnParseDocument,OnWebPagePrerender,OnLoadWebPageCache,OnRichTextEditorRegister,OnRichTextEditorInit,OnInterfaceSettingsRender\n * @internal    @modx_category Manager and Admin\n * @internal    @legacy_names TinyMCE4\n * @internal    @installset base\n * @logo        /assets/plugins/tinymce4/tinymce/logo.png\n * @reportissues https://github.com/extras-evolution/tinymce4-for-modx-evo\n * @documentation Plugin docs https://github.com/extras-evolution/tinymce4-for-modx-evo\n * @documentation Official TinyMCE4-docs https://www.tinymce.com/docs/\n * @author      Deesen\n * @lastupdate  2018-01-17\n */\nif (!defined(\'MODX_BASE_PATH\')) { die(\'What are you doing? Get out of here!\'); }\n\nrequire(MODX_BASE_PATH.\"assets/plugins/tinymce4/plugin.tinymce.inc.php\");', 0, '{\"styleFormats\":[{\"label\":\"Custom Style Formats <b>RAW<\\/b><br\\/><br\\/><ul><li>leave empty to use below block\\/inline formats<\\/li><li>allows simple-format: <i>Title,cssClass|Title2,cssClass2<\\/i><\\/li><li>Also accepts full JSON-config as per TinyMCE4 docs \\/ configure \\/ content-formating \\/ style_formats<\\/li><\\/ul>\",\"type\":\"textarea\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"styleFormats_inline\":[{\"label\":\"Custom Style Formats <b>INLINE<\\/b><br\\/><br\\/><ul><li>will wrap selected text with span-tag + css-class<\\/li><li>simple-format only<\\/li><\\/ul>\",\"type\":\"textarea\",\"value\":\"InlineTitle,cssClass1|InlineTitle2,cssClass2\",\"default\":\"InlineTitle,cssClass1|InlineTitle2,cssClass2\",\"desc\":\"\"}],\"styleFormats_block\":[{\"label\":\"Custom Style Formats <b>BLOCK<\\/b><br\\/><br\\/><ul><li>will add css-class to selected block-element<\\/li><li>simple-format only<\\/li><\\/ul>\",\"type\":\"textarea\",\"value\":\"BlockTitle,cssClass3|BlockTitle2,cssClass4\",\"default\":\"BlockTitle,cssClass3|BlockTitle2,cssClass4\",\"desc\":\"\"}],\"customParams\":[{\"label\":\"Custom Parameters<br\\/><b>(Be careful or leave empty!)<\\/b>\",\"type\":\"textarea\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"entityEncoding\":[{\"label\":\"Entity Encoding\",\"type\":\"list\",\"value\":\"named\",\"options\":\"named,numeric,raw\",\"default\":\"named\",\"desc\":\"\"}],\"entities\":[{\"label\":\"Entities\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"pathOptions\":[{\"label\":\"Path Options\",\"type\":\"list\",\"value\":\"Site config\",\"options\":\"Site config,Absolute path,Root relative,URL,No convert\",\"default\":\"Site config\",\"desc\":\"\"}],\"resizing\":[{\"label\":\"Advanced Resizing\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"disabledButtons\":[{\"label\":\"Disabled Buttons\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"webTheme\":[{\"label\":\"Web Theme\",\"type\":\"test\",\"value\":\"webuser\",\"default\":\"webuser\",\"desc\":\"\"}],\"webPlugins\":[{\"label\":\"Web Plugins\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"webButtons1\":[{\"label\":\"Web Buttons 1\",\"type\":\"text\",\"value\":\"bold italic underline strikethrough removeformat alignleft aligncenter alignright\",\"default\":\"bold italic underline strikethrough removeformat alignleft aligncenter alignright\",\"desc\":\"\"}],\"webButtons2\":[{\"label\":\"Web Buttons 2\",\"type\":\"text\",\"value\":\"link unlink image undo redo\",\"default\":\"link unlink image undo redo\",\"desc\":\"\"}],\"webButtons3\":[{\"label\":\"Web Buttons 3\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"webButtons4\":[{\"label\":\"Web Buttons 4\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"webAlign\":[{\"label\":\"Web Toolbar Alignment\",\"type\":\"list\",\"value\":\"ltr\",\"options\":\"ltr,rtl\",\"default\":\"ltr\",\"desc\":\"\"}],\"width\":[{\"label\":\"Width\",\"type\":\"text\",\"value\":\"100%\",\"default\":\"100%\",\"desc\":\"\"}],\"height\":[{\"label\":\"Height\",\"type\":\"text\",\"value\":\"400px\",\"default\":\"400px\",\"desc\":\"\"}],\"introtextRte\":[{\"label\":\"<b>Introtext RTE<\\/b><br\\/>add richtext-features to \\\"introtext\\\"\",\"type\":\"list\",\"value\":\"disabled\",\"options\":\"enabled,disabled\",\"default\":\"disabled\",\"desc\":\"\"}],\"inlineMode\":[{\"label\":\"<b>Inline-Mode<\\/b>\",\"type\":\"list\",\"value\":\"disabled\",\"options\":\"enabled,disabled\",\"default\":\"disabled\",\"desc\":\"\"}],\"inlineTheme\":[{\"label\":\"<b>Inline-Mode<\\/b><br\\/>Theme\",\"type\":\"text\",\"value\":\"inline\",\"default\":\"inline\",\"desc\":\"\"}],\"browser_spellcheck\":[{\"label\":\"<b>Browser Spellcheck<\\/b><br\\/>At least one dictionary must be installed inside your browser\",\"type\":\"list\",\"value\":\"disabled\",\"options\":\"enabled,disabled\",\"default\":\"disabled\",\"desc\":\"\"}],\"paste_as_text\":[{\"label\":\"<b>Force Paste as Text<\\/b>\",\"type\":\"list\",\"value\":\"disabled\",\"options\":\"enabled,disabled\",\"default\":\"disabled\",\"desc\":\"\"}]}', 0, '', 0, 0),
(10, 'OutdatedExtrasCheck', '<strong>1.4.0</strong> Check for Outdated critical extras not compatible with EVO 1.4.0', 0, 4, 0, '/**\n * OutdatedExtrasCheck\n *\n * Check for Outdated critical extras not compatible with EVO 1.4.0\n *\n * @category	plugin\n * @version     1.4.0 \n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @package     evo\n * @author      Author: Nicola Lambathakis\n * @internal    @events OnManagerWelcomeHome\n * @internal    @properties &wdgVisibility=Show widget for:;menu;All,AdminOnly,AdminExcluded,ThisRoleOnly,ThisUserOnly;All &ThisRole=Run only for this role:;string;;;(role id) &ThisUser=Run only for this user:;string;;;(username) &DittoVersion=Min Ditto version:;string;2.1.3 &EformVersion=Min eForm version:;string;1.4.9 &AjaxSearchVersion=Min AjaxSearch version:;string;1.11.0 &WayfinderVersion=Min Wayfinder version:;string;2.0.5 &WebLoginVersion=Min WebLogin version:;string;1.2 &WebSignupVersion=Min WebSignup version:;string;1.1.2 &WebChangePwdVersion=Min WebChangePwd version:;string;1.1.2 &BreadcrumbsVersion=Min Breadcrumbs version:;string;1.0.5 &ReflectVersion=Min Reflect version:;string;2.2 &JotVersion=Min Jot version:;string;1.1.5 &MtvVersion=Min multiTV version:;string;2.0.13 &badthemes=Outdated Manager Themes:;string;MODxRE2_DropdownMenu,MODxRE2,MODxRE,MODxCarbon,D3X,MODxFLAT,wMOD,ScienceStyle\n * @internal    @modx_category Manager and Admin\n * @internal    @installset base\n * @internal    @disabled 0\n */\n\n// get manager role check\n$internalKey = $modx->getLoginUserID();\n$sid = $modx->sid;\n$role = $_SESSION[\'mgrRole\'];\n$user = $_SESSION[\'mgrShortname\'];\n// show widget only to Admin role 1\nif(($role!=1) AND ($wdgVisibility == \'AdminOnly\')) {}\n// show widget to all manager users excluded Admin role 1\nelse if(($role==1) AND ($wdgVisibility == \'AdminExcluded\')) {}\n// show widget only to \"this\" role id\nelse if(($role!=$ThisRole) AND ($wdgVisibility == \'ThisRoleOnly\')) {}\n// show widget only to \"this\" username\nelse if(($user!=$ThisUser) AND ($wdgVisibility == \'ThisUserOnly\')) {}\nelse {\n// get plugin id and setting button\n$result = $modx->db->select(\'id\', $this->getFullTableName(\"site_plugins\"), \"name=\'{$modx->event->activePlugin}\' AND disabled=0\");\n$pluginid = $modx->db->getValue($result);\nif($modx->hasPermission(\'edit_plugin\')) {\n$button_pl_config = \'<a data-toggle=\"tooltip\" href=\"javascript:;\" title=\"\' . $_lang[\"settings_config\"] . \'\" class=\"text-muted pull-right\" onclick=\"parent.modx.popup({url:\\\'\'. MODX_MANAGER_URL.\'?a=102&id=\'.$pluginid.\'&tab=1\\\',title1:\\\'\' . $_lang[\"settings_config\"] . \'\\\',icon:\\\'fa-cog\\\',iframe:\\\'iframe\\\',selector2:\\\'#tabConfig\\\',position:\\\'center center\\\',width:\\\'80%\\\',height:\\\'80%\\\',hide:0,hover:0,overlay:1,overlayclose:1})\" ><i class=\"fa fa-cog fa-spin-hover\" style=\"color:#FFFFFF;\"></i> </a>\';\n}\n$modx->setPlaceholder(\'button_pl_config\', $button_pl_config);\n//plugin lang\n$_oec_lang = array();\n$plugin_path = $modx->config[\'base_path\'] . \"assets/plugins/extrascheck/\";\ninclude($plugin_path . \'lang/english.php\');\nif (file_exists($plugin_path . \'lang/\' . $modx->config[\'manager_language\'] . \'.php\')) {\ninclude($plugin_path . \'lang/\' . $modx->config[\'manager_language\'] . \'.php\');\n}\n//run the plugin\n// get globals\nglobal $modx,$_lang;\n//function to extract snippet version from description <strong></strong> tags \nif (!function_exists(\'getver\')) {\nfunction getver($string, $tag)\n{\n$content =\"/<$tag>(.*?)<\\/$tag>/\";\npreg_match($content, $string, $text);\nreturn $text[1];\n	}\n}\n$e = &$modx->Event;\n$EVOversion = $modx->config[\'settings_version\'];\n$output = \'\';\n//get extras module id for the link\n$modtable = $modx->getFullTableName(\'site_modules\');\n$getExtra = $modx->db->select( \"id, name\", $modtable, \"name=\'Extras\'\" );\nwhile( $row = $modx->db->getRow( $getExtra ) ) {\n$ExtrasID = $row[\'id\'];\n}\n//check outdated files\n//ajax index\n$indexajax = \"../index-ajax.php\";\nif (file_exists($indexajax)){\n    $output .= \'<div class=\"widget-wrapper alert alert-danger\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>index-ajax.php</b> \'.$_oec_lang[\'not_used\'].\' <b>Evolution \'.$EVOversion.\'</b>.  \'.$_oec_lang[\'if_dont_use\'].\', \'.$_oec_lang[\'please_delete\'].\'.</div>\';\n}\n//check outdated default manager themes\n$oldthemes = explode(\",\",\"$badthemes\");\nforeach ($oldthemes as $oldtheme){\n	if (file_exists(\'media/style/\'.$oldtheme)){\n    $output .= \'<div class=\"widget-wrapper alert alert-danger\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\'.$oldtheme.\'</b> \'.$_lang[\"manager_theme\"].\',  \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>.   \'.$_oec_lang[\'please_delete\'].\' \'.$_oec_lang[\'from_folder\'].\' \' . MODX_MANAGER_PATH . \'media/style/.</div>\';\n}\n}	\n//get site snippets table\n$table = $modx->getFullTableName(\'site_snippets\');\n//check ditto\n//get min version from config\n$minDittoVersion = $DittoVersion;\n//search the snippet by name\n$CheckDitto = $modx->db->select( \"id, name, description\", $table, \"name=\'Ditto\'\" );\nif($CheckDitto != \'\'){\nwhile( $row = $modx->db->getRow( $CheckDitto ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_ditto_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_ditto_version,$minDittoVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_ditto_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minDittoVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>DocLister</b></div>\';\n		}\n	}\n} \n//end check ditto\n\n//check eform\n//get min version from config\n$minEformVersion = $EformVersion;\n//search the snippet by name\n$CheckEform = $modx->db->select( \"id, name, description\", $table, \"name=\'eForm\'\" );\nif($CheckEform != \'\'){\nwhile( $row = $modx->db->getRow( $CheckEform ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_Eform_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_Eform_version,$minEformVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_Eform_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minEformVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>FormLister</b></div>\';\n		}\n	}\n} \n//end check eform\n	\n//check AjaxSearch\n//get min version from config\n$minAjaxSearchVersion = $AjaxSearchVersion;\n//search the snippet by name\n$CheckAjaxSearch = $modx->db->select( \"id, name, description\", $table, \"name=\'AjaxSearch\'\" );\nif($CheckAjaxSearch != \'\'){\nwhile( $row = $modx->db->getRow( $CheckAjaxSearch ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_AjaxSearch_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_AjaxSearch_version,$minAjaxSearchVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_AjaxSearch_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minAjaxSearchVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';\n		}\n	}\n} \n//end check AjaxSearch	\n	\n//check Wayfinder\n//get min version from config\n$minWayfinderVersion = $WayfinderVersion;\n//search the snippet by name\n$CheckWayfinder = $modx->db->select( \"id, name, description\", $table, \"name=\'Wayfinder\'\" );\nif($CheckWayfinder != \'\'){\nwhile( $row = $modx->db->getRow( $CheckWayfinder ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_Wayfinder_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_Wayfinder_version,$minWayfinderVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_Wayfinder_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minWayfinderVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';\n		}\n	}\n} \n//end check Wayfinder\n	\n//check WebLogin\n//get min version from config\n$minWebLoginVersion = $WebLoginVersion;\n//search the snippet by name\n$CheckWebLogin = $modx->db->select( \"id, name, description\", $table, \"name=\'WebLogin\'\" );\nif($CheckWebLogin != \'\'){\nwhile( $row = $modx->db->getRow( $CheckWebLogin ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_WebLogin_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_WebLogin_version,$minWebLoginVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_WebLogin_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minWebLoginVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>FormLister</b></div>\';\n		}\n	}\n} \n//end check WebLogin\n\n//check WebChangePwd\n//get min version from config\n$minWebChangePwdVersion = $WebChangePwdVersion;\n//search the snippet by name\n$CheckWebChangePwd = $modx->db->select( \"id, name, description\", $table, \"name=\'WebChangePwd\'\" );\nif($CheckWebLogin != \'\'){\nwhile( $row = $modx->db->getRow( $CheckWebChangePwd ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_WebChangePwd_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_WebChangePwd_version,$minWebChangePwdVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_WebChangePwd_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minWebChangePwdVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>FormLister</b></div>\';\n		}\n	}\n} \n//end check WebChangePwd\n	\n//check WebSignup\n//get min version from config\n$minWebSignupVersion = $WebSignupVersion;\n//search the snippet by name\n$CheckWebSignup = $modx->db->select( \"id, name, description\", $table, \"name=\'WebSignup\'\" );\nif($CheckWebSignup != \'\'){\nwhile( $row = $modx->db->getRow( $CheckWebSignup ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_WebSignup_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_WebSignup_version,$minWebSignupVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_WebSignup_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minWebSignupVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>FormLister</b></div>\';\n		}\n	}\n} \n//end check WebSignup\n\n//check Breadcrumbs\n//get min version from config\n$minBreadcrumbsVersion = $BreadcrumbsVersion;\n//search the snippet by name\n$CheckBreadcrumbs = $modx->db->select( \"id, name, description\", $table, \"name=\'Breadcrumbs\'\" );\nif($CheckBreadcrumbs != \'\'){\nwhile( $row = $modx->db->getRow( $CheckBreadcrumbs ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_Breadcrumbs_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_Breadcrumbs_version,$minBreadcrumbsVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_Breadcrumbs_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minBreadcrumbsVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';\n		}\n	}\n} \n//end check Breadcrumbs\n\n//check Reflect\n//get min version from config\n$minReflectVersion = $ReflectVersion;\n//search the snippet by name\n$CheckReflect = $modx->db->select( \"id, name, description\", $table, \"name=\'Reflect\'\" );\nif($CheckReflect != \'\'){\nwhile( $row = $modx->db->getRow( $CheckReflect ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_Reflect_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_Reflect_version,$minReflectVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_Reflect_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minReflectVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';\n		}\n	}\n} \n//end check Reflect\n\n//check Jot\n//get min version from config\n$minJotVersion = $JotVersion;\n//search the snippet by name\n$CheckJot = $modx->db->select( \"id, name, description\", $table, \"name=\'Jot\'\" );\nif($CheckJot != \'\'){\nwhile( $row = $modx->db->getRow( $CheckJot ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_Jot_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_Jot_version,$minJotVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_Jot_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minJotVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';\n		}\n	}\n} \n//end check Jot\n	\n//check Multitv\n//get min version from config\n$minMtvVersion = $MtvVersion;\n//search the snippet by name\n$CheckMtv = $modx->db->select( \"id, name, description\", $table, \"name=\'multiTV\'\" );\nif($CheckMtv != \'\'){\nwhile( $row = $modx->db->getRow( $CheckMtv ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_mtv_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_mtv_version,$minMtvVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_mtv_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minMtvVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a></div>\';\n		}\n	}\n} \n//end check Multitv\n\nif($output != \'\'){\nif($e->name == \'OnManagerWelcomeHome\') {\n$out = $output;\n$wdgTitle = \'EVO \'.$EVOversion.\' - \'.$_oec_lang[\'title\'].\'\';\n$widgets[\'xtraCheck\'] = array(\n				\'menuindex\' =>\'0\',\n				\'id\' => \'xtraCheck\'.$pluginid.\'\',\n				\'cols\' => \'col-md-12\',\n                \'headAttr\' => \'style=\"background-color:#B60205; color:#FFFFFF;\"\',\n				\'bodyAttr\' => \'style=\"background-color:#FFFFFF; color:#24292E;\"\',\n				\'icon\' => \'fa-warning\',\n				\'title\' => \'\'.$wdgTitle.\' \'.$button_pl_config.\'\',\n				\'body\' => \'<div class=\"card-body\">\'.$out.\'</div>\',\n				\'hide\' => \'0\'\n			);	\n            $e->output(serialize($widgets));\nreturn;\n		}\n	}\n}', 0, '{\"wdgVisibility\":[{\"label\":\"Show widget for:\",\"type\":\"menu\",\"value\":\"All\",\"options\":\"All,AdminOnly,AdminExcluded,ThisRoleOnly,ThisUserOnly\",\"default\":\"All\",\"desc\":\"\"}],\"ThisRole\":[{\"label\":\"Run only for this role:\",\"type\":\"string\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"ThisUser\":[{\"label\":\"Run only for this user:\",\"type\":\"string\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"DittoVersion\":[{\"label\":\"Min Ditto version:\",\"type\":\"string\",\"value\":\"2.1.3\",\"default\":\"2.1.3\",\"desc\":\"\"}],\"EformVersion\":[{\"label\":\"Min eForm version:\",\"type\":\"string\",\"value\":\"1.4.9\",\"default\":\"1.4.9\",\"desc\":\"\"}],\"AjaxSearchVersion\":[{\"label\":\"Min AjaxSearch version:\",\"type\":\"string\",\"value\":\"1.11.0\",\"default\":\"1.11.0\",\"desc\":\"\"}],\"WayfinderVersion\":[{\"label\":\"Min Wayfinder version:\",\"type\":\"string\",\"value\":\"2.0.5\",\"default\":\"2.0.5\",\"desc\":\"\"}],\"WebLoginVersion\":[{\"label\":\"Min WebLogin version:\",\"type\":\"string\",\"value\":\"1.2\",\"default\":\"1.2\",\"desc\":\"\"}],\"WebSignupVersion\":[{\"label\":\"Min WebSignup version:\",\"type\":\"string\",\"value\":\"1.1.2\",\"default\":\"1.1.2\",\"desc\":\"\"}],\"WebChangePwdVersion\":[{\"label\":\"Min WebChangePwd version:\",\"type\":\"string\",\"value\":\"1.1.2\",\"default\":\"1.1.2\",\"desc\":\"\"}],\"BreadcrumbsVersion\":[{\"label\":\"Min Breadcrumbs version:\",\"type\":\"string\",\"value\":\"1.0.5\",\"default\":\"1.0.5\",\"desc\":\"\"}],\"ReflectVersion\":[{\"label\":\"Min Reflect version:\",\"type\":\"string\",\"value\":\"2.2\",\"default\":\"2.2\",\"desc\":\"\"}],\"JotVersion\":[{\"label\":\"Min Jot version:\",\"type\":\"string\",\"value\":\"1.1.5\",\"default\":\"1.1.5\",\"desc\":\"\"}],\"MtvVersion\":[{\"label\":\"Min multiTV version:\",\"type\":\"string\",\"value\":\"2.0.13\",\"default\":\"2.0.13\",\"desc\":\"\"}],\"badthemes\":[{\"label\":\"Outdated Manager Themes:\",\"type\":\"string\",\"value\":\"MODxRE2_DropdownMenu,MODxRE2,MODxRE,MODxCarbon,D3X,MODxFLAT,wMOD,ScienceStyle\",\"default\":\"MODxRE2_DropdownMenu,MODxRE2,MODxRE,MODxCarbon,D3X,MODxFLAT,wMOD,ScienceStyle\",\"desc\":\"\"}]}', 0, '', 0, 0),
(11, 'TransAlias', '<strong>1.0.4</strong> Human readible URL translation supporting multiple languages and overrides', 0, 4, 0, 'require MODX_BASE_PATH.\'assets/plugins/transalias/plugin.transalias.php\';', 0, '{\"table_name\":[{\"label\":\"Trans table\",\"type\":\"list\",\"value\":\"russian\",\"options\":\"common,russian,dutch,german,czech,utf8,utf8lowercase\",\"default\":\"russian\",\"desc\":\"\"}],\"char_restrict\":[{\"label\":\"Restrict alias to\",\"type\":\"list\",\"value\":\"lowercase alphanumeric\",\"options\":\"lowercase alphanumeric,alphanumeric,legal characters\",\"default\":\"lowercase alphanumeric\",\"desc\":\"\"}],\"remove_periods\":[{\"label\":\"Remove Periods\",\"type\":\"list\",\"value\":\"No\",\"options\":\"Yes,No\",\"default\":\"No\",\"desc\":\"\"}],\"word_separator\":[{\"label\":\"Word Separator\",\"type\":\"list\",\"value\":\"dash\",\"options\":\"dash,underscore,none\",\"default\":\"dash\",\"desc\":\"\"}],\"override_tv\":[{\"label\":\"Override TV name\",\"type\":\"string\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}]}', 0, '', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_plugin_events`
--

CREATE TABLE `aw36_site_plugin_events` (
  `pluginid` int(10) NOT NULL,
  `evtid` int(10) NOT NULL DEFAULT '0',
  `priority` int(10) NOT NULL DEFAULT '0' COMMENT 'determines plugin run order'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Links to system events';

--
-- Дамп данных таблицы `aw36_site_plugin_events`
--

INSERT INTO `aw36_site_plugin_events` (`pluginid`, `evtid`, `priority`) VALUES
(1, 3, 0),
(1, 13, 0),
(1, 28, 0),
(1, 31, 0),
(1, 92, 0),
(2, 70, 0),
(2, 202, 0),
(2, 1000, 0),
(3, 34, 0),
(3, 35, 0),
(3, 36, 0),
(3, 40, 0),
(3, 41, 0),
(3, 42, 0),
(4, 80, 0),
(4, 81, 0),
(4, 93, 0),
(5, 4, 0),
(5, 79, 0),
(5, 90, 0),
(5, 1000, 0),
(6, 28, 0),
(6, 29, 0),
(6, 30, 0),
(6, 31, 0),
(6, 35, 0),
(6, 53, 0),
(6, 205, 0),
(7, 23, 0),
(7, 29, 0),
(7, 35, 0),
(7, 41, 0),
(7, 47, 0),
(7, 73, 0),
(7, 88, 0),
(8, 25, 0),
(8, 27, 0),
(8, 37, 0),
(8, 39, 0),
(8, 43, 0),
(8, 45, 0),
(8, 49, 0),
(8, 51, 0),
(8, 55, 0),
(8, 57, 0),
(8, 75, 0),
(8, 77, 0),
(8, 206, 0),
(8, 210, 0),
(8, 211, 0),
(9, 3, 0),
(9, 20, 0),
(9, 85, 0),
(9, 87, 0),
(9, 88, 0),
(9, 91, 0),
(9, 92, 0),
(10, 202, 0),
(11, 100, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_snippets`
--

CREATE TABLE `aw36_site_snippets` (
  `id` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Snippet',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `snippet` mediumtext COLLATE utf8_unicode_ci,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text COLLATE utf8_unicode_ci COMMENT 'Default Properties',
  `moduleguid` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID of module from which to import shared parameters',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Disables the snippet'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains the site snippets.';

--
-- Дамп данных таблицы `aw36_site_snippets`
--

INSERT INTO `aw36_site_snippets` (`id`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `moduleguid`, `createdon`, `editedon`, `disabled`) VALUES
(1, 'DLSitemap', '<strong>1.0.0</strong> Snippet to build XML sitemap', 0, 5, 0, 'return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DLSitemap.php\';\n', 0, '', '', 0, 0, 0),
(2, 'if', '<strong>1.3</strong> A simple conditional snippet. Allows for eq/neq/lt/gt/etc logic within templates, resources, chunks, etc.', 0, 6, 0, 'return require MODX_BASE_PATH.\'assets/snippets/if/snippet.if.php\';', 0, '', '', 0, 0, 0),
(3, 'DLCrumbs', '<strong>1.2</strong> DLCrumbs', 0, 6, 0, 'return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DLCrumbs.php\';', 0, '', '', 0, 0, 0),
(4, 'summary', '<strong>2.0.2</strong> Truncates the string to the specified length', 0, 5, 0, 'return require MODX_BASE_PATH.\'assets/snippets/summary/snippet.summary.php\';', 0, '', '', 0, 0, 0),
(5, 'DLMenu', '<strong>1.3.0</strong> Snippet to build menu with DocLister', 0, 6, 0, 'return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DLMenu.php\';\n', 0, '', '', 0, 0, 0),
(6, 'DocLister', '<strong>2.3.14</strong> Snippet to display the information of the tables by the description rules. The main goal - replacing Ditto and CatalogView', 0, 5, 0, 'return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DocLister.php\';\n', 0, '', '', 0, 0, 0),
(7, 'DocInfo', '<strong>1</strong> Берем любое поле из любого документа (меньше запросов по сравнению с GetField)', 0, 5, 0, 'return require MODX_BASE_PATH.\'assets/snippets/docinfo/snippet.docinfo.php\';', 0, '', '', 0, 0, 0),
(8, 'FormLister', '<strong>1.7.18</strong> Form processor', 0, 5, 0, 'return require MODX_BASE_PATH.\'assets/snippets/FormLister/snippet.FormLister.php\';\n', 0, '', '', 0, 0, 0),
(9, 'phpthumb', '<strong>1.3</strong> PHPThumb creates thumbnails and altered images on the fly and caches them', 0, 5, 0, 'return require MODX_BASE_PATH.\'assets/snippets/phpthumb/snippet.phpthumb.php\';\n', 0, '', '', 0, 0, 0),
(10, 'eForm', '<strong>1.4.9</strong> Robust form parser/processor with validation, multiple sending options, chunk/page support for forms and reports, and file uploads', 0, 7, 0, 'return require MODX_BASE_PATH.\'assets/snippets/eform/snippet.eform.php\';\n', 0, '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_templates`
--

CREATE TABLE `aw36_site_templates` (
  `id` int(10) NOT NULL,
  `templatename` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Template',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'url to icon file',
  `template_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-page,1-content',
  `content` mediumtext COLLATE utf8_unicode_ci,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `selectable` tinyint(4) NOT NULL DEFAULT '1',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains the site templates.';

--
-- Дамп данных таблицы `aw36_site_templates`
--

INSERT INTO `aw36_site_templates` (`id`, `templatename`, `description`, `editor_type`, `category`, `icon`, `template_type`, `content`, `locked`, `selectable`, `createdon`, `editedon`) VALUES
(3, 'Главная', '', 0, 0, '', 0, '<!DOCTYPE html>\r\n<html lang=\"en\">\r\n\r\n	{{head}}\r\n\r\n	<body>\r\n		<header>\r\n\r\n			<div class=\"container\">\r\n				<nav class=\"navbar navbar-light\">\r\n					<a class=\"navbar-brand\" href=\"#\">\r\n						<img src=\"assets\\templates\\img/logo.png\" class=\"d-inline-block align-top\" alt=\"\">\r\n					</a>\r\n					<ul class=\"nav justify-content-end header-nav-links\">\r\n						<li class=\"nav-item\">\r\n							<a class=\"nav-link\" href=\"#\">Главная</a>\r\n						</li>\r\n						<li class=\"nav-item\">\r\n							<a class=\"nav-link\" href=\"#tov-items\">Продукция</a>\r\n						</li>\r\n						<li class=\"nav-item\">\r\n							<a class=\"nav-link\" href=\"#call-us\">Контакты</a>\r\n						</li>\r\n					</ul>\r\n				</nav>\r\n			</div>\r\n			<div class=\"container\" style=\"position: relative;bottom: 50px;\">\r\n				<div class=\"row\">\r\n					<div data-wow-delay=\"0.5s\" data-wow-duration=\"0.9s\" class=\"wow fadeInRight offset-md-9 col-md-3\">\r\n						<div class=\"header-slick\">\r\n							<img src=\"assets\\images\\slajd-1.png\" alt=\"\">\r\n							<img src=\"assets\\images\\slajd-2.png\" alt=\"\">\r\n							<img src=\"assets\\images\\slajd-3.png\" alt=\"\">\r\n							<img src=\"assets\\images\\slajd-4.png\" alt=\"\">\r\n						</div>\r\n					</div>\r\n\r\n				</div>\r\n\r\n			</div>\r\n		</header>\r\n		<section id=\"production\">\r\n			<div class=\"container\">\r\n				<div>\r\n					<div class=\"item\">\r\n						<div class=\"image wow fadeInLeft\" data-wow-delay=\"0.3s\" data-wow-duration=\"0.9s\">\r\n							<img src=\"assets\\images\\slajd-2.png\" style=\"width: 350px;\" alt=\"\">\r\n						</div>\r\n						<div class=\"wow fadeInRight\" data-wow-delay=\"0.3s\" data-wow-duration=\"0.9s\">\r\n							<img class=\"text\" src=\"assets\\images\\tekst.png\" alt=\"\">\r\n						</div>\r\n						<div class=\"clearfix\"></div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</section>\r\n		<section id=\"tov-items\" class=\"tov-items\">\r\n			<div class=\"container\">\r\n				<p class=\"firm-name\">Мяскон</p>\r\n				<h2 class=\"section-name\">Продукция</h2>\r\n				<div class=\"row\">\r\n					<div data-wow-delay=\"0.5s\" data-wow-duration=\"0.9s\" class=\"wow fadeInLeft product-item\">\r\n						<div class=\"product-img\">\r\n							<img src=\"assets\\templates\\img/product-img-1.png\" alt=\"\">\r\n						</div>\r\n						<div class=\"product-text\">\r\n							<a class=\"product-link\" href=\"[~3~]\">Тушенки</a>\r\n						</div>\r\n					</div>\r\n					<div data-wow-delay=\"0.8s\" data-wow-duration=\"0.9s\" class=\"wow fadeInLeft product-item\">\r\n						<div class=\"product-img\">\r\n							<img src=\"assets\\templates\\img/product-img-2.png\" alt=\"\">\r\n						</div>\r\n						<div class=\"product-text\">\r\n							<a class=\"product-link\" href=\"[~7~]\">Паштеты</a>\r\n						</div>\r\n					</div>\r\n					<div data-wow-delay=\"0.8s\" data-wow-duration=\"0.9s\" class=\"wow fadeInRight product-item\">\r\n						<div class=\"product-img\">\r\n							<img src=\"assets\\templates\\img/product-img-3.png\" alt=\"\">\r\n						</div>\r\n						<div class=\"product-text\">\r\n							<a class=\"product-link\" href=\"[~5~]\">Каши</a>\r\n						</div>\r\n					</div>\r\n					<div data-wow-delay=\"0.5s\" data-wow-duration=\"0.9s\" class=\"wow fadeInRight product-item\">\r\n						<div class=\"product-img\">\r\n							<img src=\"assets\\templates\\img/product-img-4.png\" style=\"bottom: 20px;width: 90%;\" alt=\"\">\r\n						</div>\r\n						<div class=\"product-text\">\r\n							<a class=\"product-link\" href=\"[~6~]\">Мясные закуски</a>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<div class=\"product-desc\">\r\n					<h2>Продукт высшего качества</h2>\r\n					<p>Неослабевающий спрос на консервы ТМ Гродфуд обеспечен их стабильным и высоким качеством — все они изготавливаются\r\n						строго по ГОСТу, а интерес к ним на рынке постоянно «подогревается» новыми акцентами-находками маркетинговой\r\n						службы компании, которые непременно становятся новыми «стандартами» для всех производителей мясной консервации.\r\n						На сегодняшний день это уже и обязательное использование банки с ключом easy-open и акцентированное обозначение\r\n						на упаковке содержания мяса в 97,5%. </p>\r\n				</div>\r\n			</div>\r\n		</section>\r\n		<section id=\"call-us\" class=\"call-us\">\r\n			<div class=\"container\">\r\n				<p class=\"firm-name\">Мяскон</p>\r\n				<div class=\"row\">\r\n					<div class=\"col-md-4\">\r\n						<h2 class=\"section-name\">Контакты</h2>\r\n						<img src=\"assets\\templates\\img/logo.png\" class=\"footer-logo\" alt=\"\">\r\n						<p>Производитель: СООО «КВИНФУД»,\r\n							Республика Беларусь, 230005,\r\n							г. Гродно, ул. Мясницкая, 16,\r\n							тел.: +375 29 3717971,\r\n							<a href=\"mailto:export3@grodfood.by\">export3@grodfood.by</a>\r\n							<a href=\"mailto:export@grodfood.by\">export@grodfood.by</a> \r\n						</p>\r\n					</div>\r\n					<div class=\"offset-md-1 col-md-7\">\r\n						<h2 class=\"section-name\">Напишите нам</h2>\r\n						[!eForm?\r\n						&formid=`feedbackForm`\r\n						&subject=`Сообщение с сайта`\r\n						&tpl=`<form action=\"/\" method=\"post\" action=\"[~[*id*]~]\">\r\n						<input type=\"hidden\" name=\"formid\" value=\"feedbackForm\" />\r\n						<input value=\"\" name=\"special\" class=\"special\" type=\"text\" eform=\"Спец:date:0\"  style=\"display:none;\" />\r\n						<div class=\"form-row\">\r\n							<div class=\"form-group col-md-6\">\r\n								<label for=\"name\">Имя Фамилия *</label>\r\n								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" required>\r\n							</div>\r\n							<div class=\"form-group col-md-6\">\r\n								<label for=\"phone\">Номер телефона *</label>\r\n								<input required type=\"text\" id=\"phone\" name=\"phone\" class=\"form-control\" >\r\n							</div>\r\n						</div>\r\n						<div class=\"form-group\">\r\n							<label for=\"email\">E-mail *</label>\r\n							<input type=\"text\" id=\"email\" name=\"email\" class=\"form-control\" required>\r\n						</div>\r\n						<div class=\"form-group\">\r\n							<label for=\"text\">Текст *</label>\r\n							<textarea type=\"text\" id=\"text\" name=\"text\" class=\"form-control\" required></textarea>\r\n						</div>\r\n						<input type=\"submit\" class=\"product-link\">\r\n						</form>`\r\n						&report=`<p>Прислано человеком, с именем: [+name+] . Подробности ниже:</p>\r\n						<table>\r\n							<tr valign=\"top\"><td>Имя:</td><td>[+name+]</td></tr>\r\n							<tr valign=\"top\"><td>E-mail:</td><td>[+email+]</td></tr>\r\n							<tr valign=\"top\"><td>Номер телефона:</td><td>[+phone+]</td></tr>\r\n							<tr valign=\"top\"><td>Текст сообщения:</td><td>[+text+]</td></tr>\r\n						</table>\r\n						<p>Можно использовать ссылку для ответа: <a href=\"mailto:[+email+]?subject=RE:[+subject+]\">[+email+]</a></p>\r\n\r\n						`\r\n						&gotoid=`[*id*]`\r\n						&vericode=`0`\r\n						&to=`ignatov.d43@gmail.com, art5887379@gmail.com`!] \r\n					</div>\r\n				</div>\r\n			</div>\r\n		</section>\r\n		{{footer}}\r\n	</body>\r\n</html>', 0, 1, 0, 1524817867),
(5, 'Продукт', '', 0, 0, '', 0, '<!DOCTYPE html>\r\n<html lang=\"en\">\r\n\r\n	{{head}}\r\n\r\n	<body>\r\n		<header style=\"background-image:url(\'[*imgback*]\');\">\r\n\r\n			<div class=\"container\">\r\n				<nav class=\"navbar navbar-light\">\r\n					<a class=\"navbar-brand\" href=\"#\">\r\n						<img src=\"assets\\templates\\img/logo.png\" class=\"d-inline-block align-top\" alt=\"\">\r\n					</a>\r\n					<ul class=\"nav justify-content-end header-nav-links\">\r\n						<li class=\"nav-item\">\r\n							<a class=\"nav-link\" href=\"#\">Главная</a>\r\n						</li>\r\n						<li class=\"nav-item\">\r\n							<a class=\"nav-link\" href=\"#tov-items\">Продукция</a>\r\n						</li>\r\n						<li class=\"nav-item\">\r\n							<a class=\"nav-link\" href=\"#call-us\">Контакты</a>\r\n						</li>\r\n					</ul>\r\n				</nav>\r\n			</div>\r\n		</header>\r\n		<section id=\"products\">\r\n	<div class=\"container\">\r\n		<p class=\"firm-name\">Мяскон</p>\r\n		<h2 class=\"section-name\">[*pagetitle*]</h2>\r\n		<p>[*introtext*]</p>\r\n		[*content*]\r\n	</div>\r\n</section>\r\n\r\n		{{footer}}\r\n	</body>\r\n</html>', 0, 0, 0, 1524765306),
(4, 'Продукция', '', 0, 0, '', 0, '<!DOCTYPE html>\r\n<html lang=\"en\">\r\n\r\n	{{head}}\r\n\r\n	<body>\r\n		<header style=\"background-image:url(\'[*imgback*]\');\">\r\n\r\n			<div class=\"container\">\r\n				<nav class=\"navbar navbar-light\">\r\n					<a class=\"navbar-brand\" href=\"#\">\r\n						<img src=\"assets\\templates\\img/logo.png\" class=\"d-inline-block align-top\" alt=\"\">\r\n					</a>\r\n					<ul class=\"nav justify-content-end header-nav-links\">\r\n						<li class=\"nav-item\">\r\n							<a class=\"nav-link\" href=\"#\">Главная</a>\r\n						</li>\r\n						<li class=\"nav-item\">\r\n							<a class=\"nav-link\" href=\"#tov-items\">Продукция</a>\r\n						</li>\r\n						<li class=\"nav-item\">\r\n							<a class=\"nav-link\" href=\"#call-us\">Контакты</a>\r\n						</li>\r\n					</ul>\r\n				</nav>\r\n			</div>\r\n		</header>\r\n		<section id=\"products\">\r\n	<div class=\"container\">\r\n		<p class=\"firm-name\">Мяскон</p>\r\n		<h2 class=\"section-name\">[*pagetitle*]</h2>\r\n		<p style=\"line-height: 24px;\">[*introtext*]</p>\r\n		[*content*]\r\n	</div>\r\n</section>\r\n\r\n		{{footer}}\r\n	</body>\r\n</html>', 0, 0, 0, 1524765309),
(6, 'Продукция (родитель)', '', 0, 0, '', 0, '<!DOCTYPE html>\r\n<html lang=\"en\">\r\n\r\n	{{head}}\r\n\r\n	<body>\r\n		<header style=\"background-image:url(\'[*imgback*]\');\">\r\n\r\n			<div class=\"container\">\r\n				<nav class=\"navbar navbar-light\">\r\n					<a class=\"navbar-brand\" href=\"#\">\r\n						<img src=\"assets\\templates\\img/logo.png\" class=\"d-inline-block align-top\" alt=\"\">\r\n					</a>\r\n					<ul class=\"nav justify-content-end header-nav-links\">\r\n						<li class=\"nav-item\">\r\n							<a class=\"nav-link\" href=\"#\">Главная</a>\r\n						</li>\r\n						<li class=\"nav-item\">\r\n							<a class=\"nav-link\" href=\"#tov-items\">Продукция</a>\r\n						</li>\r\n						<li class=\"nav-item\">\r\n							<a class=\"nav-link\" href=\"#call-us\">Контакты</a>\r\n						</li>\r\n					</ul>\r\n				</nav>\r\n			</div>\r\n		</header>\r\n		<section id=\"products\">\r\n			<div class=\"container\">\r\n				<p class=\"firm-name\">Мяскон</p>\r\n				<h2 class=\"section-name\">[*pagetitle*]</h2>\r\n				<p>[*introtext*]</p>\r\n				<div class=\"row\">\r\n					[!DocLister?\r\n					&tpl=`@CODE:\r\n					<div class=\"col-md-4 col-sm-2 pi\">\r\n						<div class=\"img-tov1\">\r\n							<img src=\"[+tv.img-prod+]\" alt=\"[+pagetitle+]\">\r\n						</div>\r\n						<p class=\"pagetitle\">[+pagetitle+]</p>\r\n						<p class=\"mass\">Массовая доля при закладке 97%</p>\r\n						<p class=\"ves\">Масса нетто г. 384</p>\r\n						<p class=\"srok_god\">Срок готности 24 мес.</p>\r\n					</div>`\r\n					&order=`ASC`\r\n					&tvList=`img-prod`\r\n					!]\r\n				</div>\r\n			</div>\r\n		</section>\r\n\r\n		{{footer}}\r\n	</body>\r\n</html>', 0, 0, 0, 1524821010);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_tmplvars`
--

CREATE TABLE `aw36_site_tmplvars` (
  `id` int(11) NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `caption` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `elements` text COLLATE utf8_unicode_ci,
  `rank` int(11) NOT NULL DEFAULT '0',
  `display` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Display Control',
  `display_params` text COLLATE utf8_unicode_ci COMMENT 'Display Control Properties',
  `default_text` text COLLATE utf8_unicode_ci,
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Site Template Variables';

--
-- Дамп данных таблицы `aw36_site_tmplvars`
--

INSERT INTO `aw36_site_tmplvars` (`id`, `type`, `name`, `caption`, `description`, `editor_type`, `category`, `locked`, `elements`, `rank`, `display`, `display_params`, `default_text`, `createdon`, `editedon`) VALUES
(1, 'text', 'titl', 'Meta title', 'Meta title', 0, 1, 0, '', 0, '', '', '[*pagetitle*] - [(site_name)]', 0, 1522868186),
(2, 'text', 'keyw', 'Meta keywords', 'Meta keywords', 0, 1, 0, '', 0, '', '', '[*pagetitle*]', 0, 1522868181),
(3, 'textarea', 'desc', 'Meta description', 'Meta description', 0, 1, 0, '', 0, '', '', '[*introtext*]', 0, 1522868177),
(4, 'checkbox', 'noIndex', 'No index page', 'Meta robots', 0, 1, 0, 'Нет==<meta name=\"robots\" content=\"noindex, nofollow\">', 0, '', '', '', 0, 1522868184),
(5, 'image', 'imgback', 'Картинка под меню', '', 0, 0, 0, '', 0, '', '', '', 1524305485, 1524305524),
(6, 'image', 'img-prod', 'Картинка ппродукта', '', 0, 0, 0, '', 0, '', '', '', 1524506606, 1524506606),
(7, 'image', 'img_cat', 'Картинка подкатегории', '', 0, 0, 0, '', 0, '', '', '', 1524506759, 1524507228);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_tmplvar_access`
--

CREATE TABLE `aw36_site_tmplvar_access` (
  `id` int(10) NOT NULL,
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for template variable access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_tmplvar_contentvalues`
--

CREATE TABLE `aw36_site_tmplvar_contentvalues` (
  `id` int(11) NOT NULL,
  `tmplvarid` int(10) NOT NULL DEFAULT '0' COMMENT 'Template Variable id',
  `contentid` int(10) NOT NULL DEFAULT '0' COMMENT 'Site Content Id',
  `value` mediumtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Site Template Variables Content Values Link Table';

--
-- Дамп данных таблицы `aw36_site_tmplvar_contentvalues`
--

INSERT INTO `aw36_site_tmplvar_contentvalues` (`id`, `tmplvarid`, `contentid`, `value`) VALUES
(2, 5, 3, 'assets/images/banner.jpg'),
(3, 5, 5, 'assets/images/banner-kashi(1).jpg'),
(4, 5, 6, 'assets/images/banner-myasnye-zakuski.jpg'),
(5, 5, 7, 'assets/images/banner-pashtety.jpg'),
(6, 7, 9, 'assets/images/slajd-1.png'),
(7, 7, 10, 'assets/images/banka-2.jpg'),
(8, 7, 11, 'assets/images/slajd-21.png'),
(9, 7, 12, 'assets/images/slajd-41.png'),
(10, 5, 9, 'assets/images/banner-govyadina(1).jpg'),
(11, 6, 8, 'assets/images/3d-banki_0002_grodfood_konserva-500g_goviadina.jpg'),
(12, 6, 13, 'assets/images/3d-banki_0004_tushenaya-govyadina-340g-myaskon.jpg'),
(13, 6, 14, 'assets/images/3d-banki_0008_banka_350g-9.jpg'),
(14, 6, 15, 'assets/images/3d-banki_0010_banka_350g-7.jpg'),
(15, 6, 16, 'assets/images/3d-banki_0013_banka_350g-4.jpg'),
(16, 6, 17, 'assets/images/3d-banki_0015_banka_350g-2.jpg'),
(17, 5, 11, 'assets/images/banner-indejka.jpg'),
(18, 5, 12, 'assets/images/banner-konina.jpg'),
(19, 5, 18, 'assets/images/banner-cyplenok.jpg'),
(20, 7, 18, 'assets/images/3d-banki_0008_banka_350g-cyplenok.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_site_tmplvar_templates`
--

CREATE TABLE `aw36_site_tmplvar_templates` (
  `tmplvarid` int(10) NOT NULL DEFAULT '0' COMMENT 'Template Variable id',
  `templateid` int(11) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Site Template Variables Templates Link Table';

--
-- Дамп данных таблицы `aw36_site_tmplvar_templates`
--

INSERT INTO `aw36_site_tmplvar_templates` (`tmplvarid`, `templateid`, `rank`) VALUES
(3, 3, 0),
(2, 3, 0),
(4, 3, 0),
(1, 3, 0),
(3, 4, 0),
(2, 4, 0),
(4, 4, 0),
(1, 4, 0),
(5, 4, 0),
(3, 5, 0),
(2, 5, 0),
(4, 5, 0),
(1, 5, 0),
(5, 5, 0),
(6, 5, 0),
(7, 4, 0),
(3, 6, 0),
(2, 6, 0),
(4, 6, 0),
(1, 6, 0),
(5, 6, 0),
(7, 6, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_system_eventnames`
--

CREATE TABLE `aw36_system_eventnames` (
  `id` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `service` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'System Service number',
  `groupname` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='System Event Names.';

--
-- Дамп данных таблицы `aw36_system_eventnames`
--

INSERT INTO `aw36_system_eventnames` (`id`, `name`, `service`, `groupname`) VALUES
(1, 'OnDocPublished', 5, ''),
(2, 'OnDocUnPublished', 5, ''),
(3, 'OnWebPagePrerender', 5, ''),
(4, 'OnWebLogin', 3, ''),
(5, 'OnBeforeWebLogout', 3, ''),
(6, 'OnWebLogout', 3, ''),
(7, 'OnWebSaveUser', 3, ''),
(8, 'OnWebDeleteUser', 3, ''),
(9, 'OnWebChangePassword', 3, ''),
(10, 'OnWebCreateGroup', 3, ''),
(11, 'OnManagerLogin', 2, ''),
(12, 'OnBeforeManagerLogout', 2, ''),
(13, 'OnManagerLogout', 2, ''),
(14, 'OnManagerSaveUser', 2, ''),
(15, 'OnManagerDeleteUser', 2, ''),
(16, 'OnManagerChangePassword', 2, ''),
(17, 'OnManagerCreateGroup', 2, ''),
(18, 'OnBeforeCacheUpdate', 4, ''),
(19, 'OnCacheUpdate', 4, ''),
(107, 'OnMakePageCacheKey', 4, ''),
(20, 'OnLoadWebPageCache', 4, ''),
(21, 'OnBeforeSaveWebPageCache', 4, ''),
(22, 'OnChunkFormPrerender', 1, 'Chunks'),
(23, 'OnChunkFormRender', 1, 'Chunks'),
(24, 'OnBeforeChunkFormSave', 1, 'Chunks'),
(25, 'OnChunkFormSave', 1, 'Chunks'),
(26, 'OnBeforeChunkFormDelete', 1, 'Chunks'),
(27, 'OnChunkFormDelete', 1, 'Chunks'),
(28, 'OnDocFormPrerender', 1, 'Documents'),
(29, 'OnDocFormRender', 1, 'Documents'),
(30, 'OnBeforeDocFormSave', 1, 'Documents'),
(31, 'OnDocFormSave', 1, 'Documents'),
(32, 'OnBeforeDocFormDelete', 1, 'Documents'),
(33, 'OnDocFormDelete', 1, 'Documents'),
(1033, 'OnDocFormUnDelete', 1, 'Documents'),
(1034, 'onBeforeMoveDocument', 1, 'Documents'),
(1035, 'onAfterMoveDocument', 1, 'Documents'),
(34, 'OnPluginFormPrerender', 1, 'Plugins'),
(35, 'OnPluginFormRender', 1, 'Plugins'),
(36, 'OnBeforePluginFormSave', 1, 'Plugins'),
(37, 'OnPluginFormSave', 1, 'Plugins'),
(38, 'OnBeforePluginFormDelete', 1, 'Plugins'),
(39, 'OnPluginFormDelete', 1, 'Plugins'),
(40, 'OnSnipFormPrerender', 1, 'Snippets'),
(41, 'OnSnipFormRender', 1, 'Snippets'),
(42, 'OnBeforeSnipFormSave', 1, 'Snippets'),
(43, 'OnSnipFormSave', 1, 'Snippets'),
(44, 'OnBeforeSnipFormDelete', 1, 'Snippets'),
(45, 'OnSnipFormDelete', 1, 'Snippets'),
(46, 'OnTempFormPrerender', 1, 'Templates'),
(47, 'OnTempFormRender', 1, 'Templates'),
(48, 'OnBeforeTempFormSave', 1, 'Templates'),
(49, 'OnTempFormSave', 1, 'Templates'),
(50, 'OnBeforeTempFormDelete', 1, 'Templates'),
(51, 'OnTempFormDelete', 1, 'Templates'),
(52, 'OnTVFormPrerender', 1, 'Template Variables'),
(53, 'OnTVFormRender', 1, 'Template Variables'),
(54, 'OnBeforeTVFormSave', 1, 'Template Variables'),
(55, 'OnTVFormSave', 1, 'Template Variables'),
(56, 'OnBeforeTVFormDelete', 1, 'Template Variables'),
(57, 'OnTVFormDelete', 1, 'Template Variables'),
(58, 'OnUserFormPrerender', 1, 'Users'),
(59, 'OnUserFormRender', 1, 'Users'),
(60, 'OnBeforeUserFormSave', 1, 'Users'),
(61, 'OnUserFormSave', 1, 'Users'),
(62, 'OnBeforeUserFormDelete', 1, 'Users'),
(63, 'OnUserFormDelete', 1, 'Users'),
(64, 'OnWUsrFormPrerender', 1, 'Web Users'),
(65, 'OnWUsrFormRender', 1, 'Web Users'),
(66, 'OnBeforeWUsrFormSave', 1, 'Web Users'),
(67, 'OnWUsrFormSave', 1, 'Web Users'),
(68, 'OnBeforeWUsrFormDelete', 1, 'Web Users'),
(69, 'OnWUsrFormDelete', 1, 'Web Users'),
(70, 'OnSiteRefresh', 1, ''),
(71, 'OnFileManagerUpload', 1, ''),
(72, 'OnModFormPrerender', 1, 'Modules'),
(73, 'OnModFormRender', 1, 'Modules'),
(74, 'OnBeforeModFormDelete', 1, 'Modules'),
(75, 'OnModFormDelete', 1, 'Modules'),
(76, 'OnBeforeModFormSave', 1, 'Modules'),
(77, 'OnModFormSave', 1, 'Modules'),
(78, 'OnBeforeWebLogin', 3, ''),
(79, 'OnWebAuthentication', 3, ''),
(80, 'OnBeforeManagerLogin', 2, ''),
(81, 'OnManagerAuthentication', 2, ''),
(82, 'OnSiteSettingsRender', 1, 'System Settings'),
(83, 'OnFriendlyURLSettingsRender', 1, 'System Settings'),
(84, 'OnUserSettingsRender', 1, 'System Settings'),
(85, 'OnInterfaceSettingsRender', 1, 'System Settings'),
(86, 'OnMiscSettingsRender', 1, 'System Settings'),
(87, 'OnRichTextEditorRegister', 1, 'RichText Editor'),
(88, 'OnRichTextEditorInit', 1, 'RichText Editor'),
(89, 'OnManagerPageInit', 2, ''),
(90, 'OnWebPageInit', 5, ''),
(101, 'OnLoadDocumentObject', 5, ''),
(104, 'OnBeforeLoadDocumentObject', 5, ''),
(105, 'OnAfterLoadDocumentObject', 5, ''),
(91, 'OnLoadWebDocument', 5, ''),
(92, 'OnParseDocument', 5, ''),
(106, 'OnParseProperties', 5, ''),
(108, 'OnBeforeParseParams', 5, ''),
(93, 'OnManagerLoginFormRender', 2, ''),
(94, 'OnWebPageComplete', 5, ''),
(95, 'OnLogPageHit', 5, ''),
(96, 'OnBeforeManagerPageInit', 2, ''),
(97, 'OnBeforeEmptyTrash', 1, 'Documents'),
(98, 'OnEmptyTrash', 1, 'Documents'),
(99, 'OnManagerLoginFormPrerender', 2, ''),
(100, 'OnStripAlias', 1, 'Documents'),
(102, 'OnMakeDocUrl', 5, ''),
(103, 'OnBeforeLoadExtension', 5, ''),
(200, 'OnCreateDocGroup', 1, 'Documents'),
(201, 'OnManagerWelcomePrerender', 2, ''),
(202, 'OnManagerWelcomeHome', 2, ''),
(203, 'OnManagerWelcomeRender', 2, ''),
(204, 'OnBeforeDocDuplicate', 1, 'Documents'),
(205, 'OnDocDuplicate', 1, 'Documents'),
(206, 'OnManagerMainFrameHeaderHTMLBlock', 2, ''),
(207, 'OnManagerPreFrameLoader', 2, ''),
(208, 'OnManagerFrameLoader', 2, ''),
(209, 'OnManagerTreeInit', 2, ''),
(210, 'OnManagerTreePrerender', 2, ''),
(211, 'OnManagerTreeRender', 2, ''),
(212, 'OnManagerNodePrerender', 2, ''),
(213, 'OnManagerNodeRender', 2, ''),
(214, 'OnManagerMenuPrerender', 2, ''),
(215, 'OnManagerTopPrerender', 2, ''),
(224, 'OnDocFormTemplateRender', 1, 'Documents'),
(999, 'OnPageUnauthorized', 1, ''),
(1000, 'OnPageNotFound', 1, ''),
(1001, 'OnFileBrowserUpload', 1, 'File Browser Events');

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_system_settings`
--

CREATE TABLE `aw36_system_settings` (
  `setting_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `setting_value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains Content Manager settings.';

--
-- Дамп данных таблицы `aw36_system_settings`
--

INSERT INTO `aw36_system_settings` (`setting_name`, `setting_value`) VALUES
('settings_version', '1.4.2'),
('manager_theme', 'default'),
('server_offset_time', '0'),
('manager_language', 'russian-UTF8'),
('modx_charset', 'UTF-8'),
('site_name', 'My Evolution Site'),
('site_start', '1'),
('error_page', '1'),
('unauthorized_page', '1'),
('site_status', '0'),
('auto_template_logic', 'sibling'),
('default_template', '3'),
('old_template', '3'),
('publish_default', '1'),
('friendly_urls', '1'),
('friendly_alias_urls', '1'),
('use_alias_path', '1'),
('cache_type', '2'),
('failed_login_attempts', '3'),
('blocked_minutes', '60'),
('use_captcha', '0'),
('emailsender', 'info@itionika.by'),
('use_editor', '1'),
('use_browser', '1'),
('fe_editor_lang', 'russian-UTF8'),
('fck_editor_toolbar', 'standard'),
('fck_editor_autolang', '0'),
('editor_css_path', ''),
('editor_css_selectors', ''),
('upload_maxsize', '10485760'),
('manager_layout', '4'),
('auto_menuindex', '1'),
('session.cookie.lifetime', '604800'),
('mail_check_timeperiod', '600'),
('manager_direction', 'ltr'),
('xhtml_urls', '0'),
('automatic_alias', '1'),
('datetime_format', 'dd-mm-YYYY'),
('warning_visibility', '0'),
('remember_last_tab', '1'),
('enable_bindings', '1'),
('seostrict', '1'),
('number_of_results', '30'),
('theme_refresher', ''),
('show_picker', '0'),
('show_newresource_btn', '0'),
('show_fullscreen_btn', '0'),
('email_sender_method', '1'),
('site_id', '5ac3d8d198ed3'),
('site_unavailable_page', ''),
('reload_site_unavailable', ''),
('site_unavailable_message', 'В настоящее время сайт недоступен.'),
('siteunavailable_message_default', 'В настоящее время сайт недоступен.'),
('enable_filter', '0'),
('enable_at_syntax', '0'),
('cache_default', '1'),
('search_default', '1'),
('custom_contenttype', 'application/rss+xml,application/pdf,application/vnd.ms-word,application/vnd.ms-excel,text/html,text/css,text/xml,text/javascript,text/plain,application/json'),
('docid_incrmnt_method', '0'),
('enable_cache', '1'),
('minifyphp_incache', '0'),
('server_protocol', 'http'),
('rss_url_news', 'http://feeds.feedburner.com/evocms-release-news'),
('track_visitors', '0'),
('top_howmany', '10'),
('friendly_url_prefix', ''),
('friendly_url_suffix', '.html'),
('make_folders', '0'),
('aliaslistingfolder', '0'),
('allow_duplicate_alias', '0'),
('use_udperms', '1'),
('udperms_allowroot', '0'),
('email_method', 'mail'),
('smtp_auth', '0'),
('smtp_secure', 'none'),
('smtp_host', 'smtp.example.com'),
('smtp_port', '25'),
('smtp_username', 'you@example.com'),
('reload_emailsubject', ''),
('emailsubject', 'Данные для авторизации'),
('emailsubject_default', 'Данные для авторизации'),
('reload_signupemail_message', ''),
('signupemail_message', 'Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации в системе управления сайтом [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация'),
('system_email_signup_default', 'Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации в системе управления сайтом [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация'),
('reload_websignupemail_message', ''),
('websignupemail_message', 'Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации на [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация'),
('system_email_websignup_default', 'Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации на [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация'),
('reload_system_email_webreminder_message', ''),
('webpwdreminder_message', 'Здравствуйте, [+uid+]!\r\n\r\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\r\n\r\n[+surl+]\r\n\r\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\r\n\r\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\r\n\r\nС уважением, Администрация'),
('system_email_webreminder_default', 'Здравствуйте, [+uid+]!\r\n\r\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\r\n\r\n[+surl+]\r\n\r\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\r\n\r\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\r\n\r\nС уважением, Администрация'),
('tree_page_click', '27'),
('use_breadcrumbs', '0'),
('global_tabs', '1'),
('group_tvs', '0'),
('resource_tree_node_name', 'pagetitle'),
('session_timeout', '15'),
('tree_show_protected', '0'),
('datepicker_offset', '-10'),
('number_of_logs', '100'),
('number_of_messages', '40'),
('which_editor', 'TinyMCE4'),
('tinymce4_theme', 'custom'),
('tinymce4_skin', 'lightgray'),
('tinymce4_skintheme', 'inlite'),
('tinymce4_template_docs', ''),
('tinymce4_template_chunks', ''),
('tinymce4_entermode', 'p'),
('tinymce4_element_format', 'xhtml'),
('tinymce4_schema', 'html5'),
('tinymce4_custom_plugins', 'advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen spellchecker insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor codesample colorpicker textpattern imagetools paste modxlink youtube'),
('tinymce4_custom_buttons1', 'undo redo | cut copy paste | searchreplace | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | styleselect'),
('tinymce4_custom_buttons2', 'link unlink anchor image media codesample table | hr removeformat | subscript superscript charmap | nonbreaking | visualchars visualblocks print preview fullscreen code formatselect'),
('tinymce4_custom_buttons3', ''),
('tinymce4_custom_buttons4', ''),
('tinymce4_blockFormats', 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3'),
('allow_eval', 'with_scan'),
('safe_functions_at_eval', 'time,date,strtotime,strftime'),
('check_files_onlogin', 'index.php\r\n.htaccess\r\nmanager/index.php\r\nmanager/includes/config.inc.php'),
('validate_referer', '1'),
('rss_url_security', 'http://feeds.feedburner.com/evocms-security-news'),
('error_reporting', '1'),
('send_errormail', '0'),
('pwd_hash_algo', 'UNCRYPT'),
('reload_captcha_words', ''),
('captcha_words', 'EVO,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote'),
('captcha_words_default', 'EVO,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote'),
('filemanager_path', '/h/miaskonby/htdocs/'),
('upload_files', 'bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff,fla,flv,swf,aac,au,avi,css,cache,doc,docx,gz,gzip,htaccess,htm,html,js,mp3,mp4,mpeg,mpg,ods,odp,odt,pdf,ppt,pptx,rar,tar,tgz,txt,wav,wmv,xls,xlsx,xml,z,zip,JPG,JPEG,PNG,GIF,svg,tpl'),
('upload_images', 'bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff,svg'),
('upload_media', 'au,avi,mp3,mp4,mpeg,mpg,wav,wmv'),
('upload_flash', 'fla,flv,swf'),
('new_file_permissions', '0644'),
('new_folder_permissions', '0755'),
('which_browser', 'mcpuk'),
('rb_webuser', '0'),
('rb_base_dir', '/h/miaskonby/htdocs/assets/'),
('rb_base_url', 'assets/'),
('clean_uploaded_filename', '1'),
('strip_image_paths', '1'),
('maxImageWidth', '2600'),
('maxImageHeight', '2200'),
('thumbWidth', '150'),
('thumbHeight', '150'),
('thumbsDir', '.thumbs'),
('jpegQuality', '90'),
('denyZipDownload', '0'),
('denyExtensionRename', '0'),
('showHiddenFiles', '0'),
('lang_code', 'ru'),
('sys_files_checksum', 'a:4:{s:29:\"/h/miaskonby/htdocs/index.php\";s:32:\"cdc22609f8a321de87c7305a478d42aa\";s:29:\"/h/miaskonby/htdocs/.htaccess\";s:32:\"b12739e14a9d18d7090427db01bd4021\";s:37:\"/h/miaskonby/htdocs/manager/index.php\";s:32:\"95efdfa243c95d259353257ebd2235b1\";s:51:\"/h/miaskonby/htdocs/manager/includes/config.inc.php\";s:32:\"b4911cda288dc2864bd51774fb81543c\";}');

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_user_attributes`
--

CREATE TABLE `aw36_user_attributes` (
  `id` int(10) NOT NULL,
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `fullname` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `role` int(10) NOT NULL DEFAULT '0',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobilephone` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 - unknown, 1 - Male 2 - female',
  `country` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `state` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `zip` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'link to photo',
  `comment` text COLLATE utf8_unicode_ci,
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains information about the backend users.';

--
-- Дамп данных таблицы `aw36_user_attributes`
--

INSERT INTO `aw36_user_attributes` (`id`, `internalKey`, `fullname`, `role`, `email`, `phone`, `mobilephone`, `blocked`, `blockeduntil`, `blockedafter`, `logincount`, `lastlogin`, `thislogin`, `failedlogincount`, `sessionid`, `dob`, `gender`, `country`, `street`, `city`, `state`, `zip`, `fax`, `photo`, `comment`, `createdon`, `editedon`) VALUES
(1, 1, 'Admin', 1, 'info@itionika.by', '', '', 0, 0, 0, 19, 1524771229, 1524820529, 2, 'cb8344ef6685920eabe3d40abb21c284', 0, 0, '', '', '', '', '', '', '', '', 0, 0),
(2, 2, '', 3, 'ignatov.d43@gmail.com', '', '', 0, 0, 0, 16, 1524822929, 1524823834, 0, 'ac8dc9117bbfa832432939f24c2491a0', 0, 0, '', '', '', '', '', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_user_messages`
--

CREATE TABLE `aw36_user_messages` (
  `id` int(10) NOT NULL,
  `type` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subject` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8_unicode_ci,
  `sender` int(10) NOT NULL DEFAULT '0',
  `recipient` int(10) NOT NULL DEFAULT '0',
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `postdate` int(20) NOT NULL DEFAULT '0',
  `messageread` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains messages for the Content Manager messaging system.';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_user_roles`
--

CREATE TABLE `aw36_user_roles` (
  `id` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `frames` int(1) NOT NULL DEFAULT '0',
  `home` int(1) NOT NULL DEFAULT '0',
  `view_document` int(1) NOT NULL DEFAULT '0',
  `new_document` int(1) NOT NULL DEFAULT '0',
  `save_document` int(1) NOT NULL DEFAULT '0',
  `publish_document` int(1) NOT NULL DEFAULT '0',
  `delete_document` int(1) NOT NULL DEFAULT '0',
  `empty_trash` int(1) NOT NULL DEFAULT '0',
  `action_ok` int(1) NOT NULL DEFAULT '0',
  `logout` int(1) NOT NULL DEFAULT '0',
  `help` int(1) NOT NULL DEFAULT '0',
  `messages` int(1) NOT NULL DEFAULT '0',
  `new_user` int(1) NOT NULL DEFAULT '0',
  `edit_user` int(1) NOT NULL DEFAULT '0',
  `logs` int(1) NOT NULL DEFAULT '0',
  `edit_parser` int(1) NOT NULL DEFAULT '0',
  `save_parser` int(1) NOT NULL DEFAULT '0',
  `edit_template` int(1) NOT NULL DEFAULT '0',
  `settings` int(1) NOT NULL DEFAULT '0',
  `credits` int(1) NOT NULL DEFAULT '0',
  `new_template` int(1) NOT NULL DEFAULT '0',
  `save_template` int(1) NOT NULL DEFAULT '0',
  `delete_template` int(1) NOT NULL DEFAULT '0',
  `edit_snippet` int(1) NOT NULL DEFAULT '0',
  `new_snippet` int(1) NOT NULL DEFAULT '0',
  `save_snippet` int(1) NOT NULL DEFAULT '0',
  `delete_snippet` int(1) NOT NULL DEFAULT '0',
  `edit_chunk` int(1) NOT NULL DEFAULT '0',
  `new_chunk` int(1) NOT NULL DEFAULT '0',
  `save_chunk` int(1) NOT NULL DEFAULT '0',
  `delete_chunk` int(1) NOT NULL DEFAULT '0',
  `empty_cache` int(1) NOT NULL DEFAULT '0',
  `edit_document` int(1) NOT NULL DEFAULT '0',
  `change_password` int(1) NOT NULL DEFAULT '0',
  `error_dialog` int(1) NOT NULL DEFAULT '0',
  `about` int(1) NOT NULL DEFAULT '0',
  `category_manager` int(1) NOT NULL DEFAULT '0',
  `file_manager` int(1) NOT NULL DEFAULT '0',
  `assets_files` int(1) NOT NULL DEFAULT '0',
  `assets_images` int(1) NOT NULL DEFAULT '0',
  `save_user` int(1) NOT NULL DEFAULT '0',
  `delete_user` int(1) NOT NULL DEFAULT '0',
  `save_password` int(11) NOT NULL DEFAULT '0',
  `edit_role` int(1) NOT NULL DEFAULT '0',
  `save_role` int(1) NOT NULL DEFAULT '0',
  `delete_role` int(1) NOT NULL DEFAULT '0',
  `new_role` int(1) NOT NULL DEFAULT '0',
  `access_permissions` int(1) NOT NULL DEFAULT '0',
  `bk_manager` int(1) NOT NULL DEFAULT '0',
  `new_plugin` int(1) NOT NULL DEFAULT '0',
  `edit_plugin` int(1) NOT NULL DEFAULT '0',
  `save_plugin` int(1) NOT NULL DEFAULT '0',
  `delete_plugin` int(1) NOT NULL DEFAULT '0',
  `new_module` int(1) NOT NULL DEFAULT '0',
  `edit_module` int(1) NOT NULL DEFAULT '0',
  `save_module` int(1) NOT NULL DEFAULT '0',
  `delete_module` int(1) NOT NULL DEFAULT '0',
  `exec_module` int(1) NOT NULL DEFAULT '0',
  `view_eventlog` int(1) NOT NULL DEFAULT '0',
  `delete_eventlog` int(1) NOT NULL DEFAULT '0',
  `new_web_user` int(1) NOT NULL DEFAULT '0',
  `edit_web_user` int(1) NOT NULL DEFAULT '0',
  `save_web_user` int(1) NOT NULL DEFAULT '0',
  `delete_web_user` int(1) NOT NULL DEFAULT '0',
  `web_access_permissions` int(1) NOT NULL DEFAULT '0',
  `view_unpublished` int(1) NOT NULL DEFAULT '0',
  `import_static` int(1) NOT NULL DEFAULT '0',
  `export_static` int(1) NOT NULL DEFAULT '0',
  `remove_locks` int(1) NOT NULL DEFAULT '0',
  `display_locks` int(1) NOT NULL DEFAULT '0',
  `change_resourcetype` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains information describing the user roles.';

--
-- Дамп данных таблицы `aw36_user_roles`
--

INSERT INTO `aw36_user_roles` (`id`, `name`, `description`, `frames`, `home`, `view_document`, `new_document`, `save_document`, `publish_document`, `delete_document`, `empty_trash`, `action_ok`, `logout`, `help`, `messages`, `new_user`, `edit_user`, `logs`, `edit_parser`, `save_parser`, `edit_template`, `settings`, `credits`, `new_template`, `save_template`, `delete_template`, `edit_snippet`, `new_snippet`, `save_snippet`, `delete_snippet`, `edit_chunk`, `new_chunk`, `save_chunk`, `delete_chunk`, `empty_cache`, `edit_document`, `change_password`, `error_dialog`, `about`, `category_manager`, `file_manager`, `assets_files`, `assets_images`, `save_user`, `delete_user`, `save_password`, `edit_role`, `save_role`, `delete_role`, `new_role`, `access_permissions`, `bk_manager`, `new_plugin`, `edit_plugin`, `save_plugin`, `delete_plugin`, `new_module`, `edit_module`, `save_module`, `delete_module`, `exec_module`, `view_eventlog`, `delete_eventlog`, `new_web_user`, `edit_web_user`, `save_web_user`, `delete_web_user`, `web_access_permissions`, `view_unpublished`, `import_static`, `export_static`, `remove_locks`, `display_locks`, `change_resourcetype`) VALUES
(2, 'Editor', 'Limited to managing content', 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1),
(3, 'Publisher', 'Editor with expanded permissions including manage users, update Elements and site settings', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1),
(1, 'Administrator', 'Site administrators have full access to all functions', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_user_settings`
--

CREATE TABLE `aw36_user_settings` (
  `user` int(11) NOT NULL,
  `setting_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `setting_value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains backend user settings.';

--
-- Дамп данных таблицы `aw36_user_settings`
--

INSERT INTO `aw36_user_settings` (`user`, `setting_name`, `setting_value`) VALUES
(2, 'allow_manager_access', '1'),
(2, 'which_browser', 'default'),
(2, 'tinymce4_custom_plugins', 'advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen spellchecker insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor codesample colorpicker textpattern imagetools paste modxlink youtube'),
(2, 'tinymce4_custom_plugins_useglobal', '0'),
(2, 'tinymce4_custom_buttons1', 'undo redo | cut copy paste | searchreplace | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | styleselect'),
(2, 'tinymce4_custom_buttons2', 'link unlink anchor image media codesample table | hr removeformat | subscript superscript charmap | nonbreaking | visualchars visualblocks print preview fullscreen code formatselect'),
(2, 'tinymce4_custom_buttons_useglobal', '0'),
(2, 'tinymce4_blockFormats', 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3'),
(2, 'tinymce4_blockFormats_useglobal', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_webgroup_access`
--

CREATE TABLE `aw36_webgroup_access` (
  `id` int(10) NOT NULL,
  `webgroup` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for web access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_webgroup_names`
--

CREATE TABLE `aw36_webgroup_names` (
  `id` int(10) NOT NULL,
  `name` varchar(245) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for web access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_web_groups`
--

CREATE TABLE `aw36_web_groups` (
  `id` int(10) NOT NULL,
  `webgroup` int(10) NOT NULL DEFAULT '0',
  `webuser` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for web access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_web_users`
--

CREATE TABLE `aw36_web_users` (
  `id` int(10) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cachepwd` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Store new unconfirmed password'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_web_user_attributes`
--

CREATE TABLE `aw36_web_user_attributes` (
  `id` int(10) NOT NULL,
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `fullname` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `role` int(10) NOT NULL DEFAULT '0',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobilephone` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 - unknown, 1 - Male 2 - female',
  `country` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `state` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `zip` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'link to photo',
  `comment` text COLLATE utf8_unicode_ci,
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains information for web users.';

-- --------------------------------------------------------

--
-- Структура таблицы `aw36_web_user_settings`
--

CREATE TABLE `aw36_web_user_settings` (
  `webuser` int(11) NOT NULL,
  `setting_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `setting_value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains web user settings.';

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `aw36_active_users`
--
ALTER TABLE `aw36_active_users`
  ADD PRIMARY KEY (`sid`);

--
-- Индексы таблицы `aw36_active_user_locks`
--
ALTER TABLE `aw36_active_user_locks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_element_id` (`elementType`,`elementId`,`sid`);

--
-- Индексы таблицы `aw36_active_user_sessions`
--
ALTER TABLE `aw36_active_user_sessions`
  ADD PRIMARY KEY (`sid`);

--
-- Индексы таблицы `aw36_categories`
--
ALTER TABLE `aw36_categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_documentgroup_names`
--
ALTER TABLE `aw36_documentgroup_names`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `aw36_document_groups`
--
ALTER TABLE `aw36_document_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_dg_id` (`document_group`,`document`),
  ADD KEY `document` (`document`),
  ADD KEY `document_group` (`document_group`);

--
-- Индексы таблицы `aw36_event_log`
--
ALTER TABLE `aw36_event_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Индексы таблицы `aw36_manager_log`
--
ALTER TABLE `aw36_manager_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_manager_users`
--
ALTER TABLE `aw36_manager_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Индексы таблицы `aw36_membergroup_access`
--
ALTER TABLE `aw36_membergroup_access`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_membergroup_names`
--
ALTER TABLE `aw36_membergroup_names`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `aw36_member_groups`
--
ALTER TABLE `aw36_member_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_group_member` (`user_group`,`member`);

--
-- Индексы таблицы `aw36_site_content`
--
ALTER TABLE `aw36_site_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `parent` (`parent`),
  ADD KEY `aliasidx` (`alias`),
  ADD KEY `typeidx` (`type`);
ALTER TABLE `aw36_site_content` ADD FULLTEXT KEY `content_ft_idx` (`pagetitle`,`description`,`content`);

--
-- Индексы таблицы `aw36_site_htmlsnippets`
--
ALTER TABLE `aw36_site_htmlsnippets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_site_modules`
--
ALTER TABLE `aw36_site_modules`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_site_module_access`
--
ALTER TABLE `aw36_site_module_access`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_site_module_depobj`
--
ALTER TABLE `aw36_site_module_depobj`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_site_plugins`
--
ALTER TABLE `aw36_site_plugins`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_site_plugin_events`
--
ALTER TABLE `aw36_site_plugin_events`
  ADD PRIMARY KEY (`pluginid`,`evtid`);

--
-- Индексы таблицы `aw36_site_snippets`
--
ALTER TABLE `aw36_site_snippets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_site_templates`
--
ALTER TABLE `aw36_site_templates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_site_tmplvars`
--
ALTER TABLE `aw36_site_tmplvars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `indx_rank` (`rank`);

--
-- Индексы таблицы `aw36_site_tmplvar_access`
--
ALTER TABLE `aw36_site_tmplvar_access`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_site_tmplvar_contentvalues`
--
ALTER TABLE `aw36_site_tmplvar_contentvalues`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_tvid_contentid` (`tmplvarid`,`contentid`),
  ADD KEY `idx_tmplvarid` (`tmplvarid`),
  ADD KEY `idx_id` (`contentid`);
ALTER TABLE `aw36_site_tmplvar_contentvalues` ADD FULLTEXT KEY `value_ft_idx` (`value`);

--
-- Индексы таблицы `aw36_site_tmplvar_templates`
--
ALTER TABLE `aw36_site_tmplvar_templates`
  ADD PRIMARY KEY (`tmplvarid`,`templateid`);

--
-- Индексы таблицы `aw36_system_eventnames`
--
ALTER TABLE `aw36_system_eventnames`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_system_settings`
--
ALTER TABLE `aw36_system_settings`
  ADD PRIMARY KEY (`setting_name`);

--
-- Индексы таблицы `aw36_user_attributes`
--
ALTER TABLE `aw36_user_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`internalKey`);

--
-- Индексы таблицы `aw36_user_messages`
--
ALTER TABLE `aw36_user_messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_user_roles`
--
ALTER TABLE `aw36_user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_user_settings`
--
ALTER TABLE `aw36_user_settings`
  ADD PRIMARY KEY (`user`,`setting_name`),
  ADD KEY `setting_name` (`setting_name`),
  ADD KEY `user` (`user`);

--
-- Индексы таблицы `aw36_webgroup_access`
--
ALTER TABLE `aw36_webgroup_access`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aw36_webgroup_names`
--
ALTER TABLE `aw36_webgroup_names`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `aw36_web_groups`
--
ALTER TABLE `aw36_web_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_group_user` (`webgroup`,`webuser`);

--
-- Индексы таблицы `aw36_web_users`
--
ALTER TABLE `aw36_web_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Индексы таблицы `aw36_web_user_attributes`
--
ALTER TABLE `aw36_web_user_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`internalKey`);

--
-- Индексы таблицы `aw36_web_user_settings`
--
ALTER TABLE `aw36_web_user_settings`
  ADD PRIMARY KEY (`webuser`,`setting_name`),
  ADD KEY `setting_name` (`setting_name`),
  ADD KEY `webuserid` (`webuser`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `aw36_active_user_locks`
--
ALTER TABLE `aw36_active_user_locks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `aw36_categories`
--
ALTER TABLE `aw36_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `aw36_documentgroup_names`
--
ALTER TABLE `aw36_documentgroup_names`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_document_groups`
--
ALTER TABLE `aw36_document_groups`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_event_log`
--
ALTER TABLE `aw36_event_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `aw36_manager_log`
--
ALTER TABLE `aw36_manager_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=989;
--
-- AUTO_INCREMENT для таблицы `aw36_manager_users`
--
ALTER TABLE `aw36_manager_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `aw36_membergroup_access`
--
ALTER TABLE `aw36_membergroup_access`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_membergroup_names`
--
ALTER TABLE `aw36_membergroup_names`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_member_groups`
--
ALTER TABLE `aw36_member_groups`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_site_content`
--
ALTER TABLE `aw36_site_content`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `aw36_site_htmlsnippets`
--
ALTER TABLE `aw36_site_htmlsnippets`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `aw36_site_modules`
--
ALTER TABLE `aw36_site_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `aw36_site_module_access`
--
ALTER TABLE `aw36_site_module_access`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_site_module_depobj`
--
ALTER TABLE `aw36_site_module_depobj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_site_plugins`
--
ALTER TABLE `aw36_site_plugins`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `aw36_site_snippets`
--
ALTER TABLE `aw36_site_snippets`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `aw36_site_templates`
--
ALTER TABLE `aw36_site_templates`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `aw36_site_tmplvars`
--
ALTER TABLE `aw36_site_tmplvars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `aw36_site_tmplvar_access`
--
ALTER TABLE `aw36_site_tmplvar_access`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_site_tmplvar_contentvalues`
--
ALTER TABLE `aw36_site_tmplvar_contentvalues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `aw36_system_eventnames`
--
ALTER TABLE `aw36_system_eventnames`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1036;
--
-- AUTO_INCREMENT для таблицы `aw36_user_attributes`
--
ALTER TABLE `aw36_user_attributes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `aw36_user_messages`
--
ALTER TABLE `aw36_user_messages`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_user_roles`
--
ALTER TABLE `aw36_user_roles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `aw36_webgroup_access`
--
ALTER TABLE `aw36_webgroup_access`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_webgroup_names`
--
ALTER TABLE `aw36_webgroup_names`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_web_groups`
--
ALTER TABLE `aw36_web_groups`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_web_users`
--
ALTER TABLE `aw36_web_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `aw36_web_user_attributes`
--
ALTER TABLE `aw36_web_user_attributes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
